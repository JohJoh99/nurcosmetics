package de.johjoh.nurcosmetics.cmd;

import java.sql.Connection;
import java.sql.SQLException;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.johjoh.nurcosmetics.api.CosmeticsAPI;
import de.johjoh.nurcosmetics.cosmetics.Cosmetic;
import de.johjoh.nurcosmetics.sql.MySQLConnectionUtil;
import de.johjoh.nurcosmetics.util.CosmeticPlayer;
import de.johjoh.nurcosmetics.util.CosmeticStorage;
import net.nurserver.core.profile.Profile;
import net.nurserver.core.profile.ProfileManager;
import net.nurserver.core.rank.Rank;

public class CosmeticsCmd implements CommandExecutor {

	public boolean onCommand(CommandSender cs, Command cmd, String name, String[] args) {
		if(!(cs instanceof Player)) return true;
		Profile pr = ProfileManager.getProfile((Player)cs);
		if(pr.getRank() != Rank.ADMINISTRATOR && pr.getRank() != Rank.DEVELOPER) return true;
		
		if(args.length == 0) {
			cs.sendMessage(ChatColor.RED + "/cosmetic <get|reset>");
			return true;
		}
		
		if(args.length > 0) {
			if(args[0].equalsIgnoreCase("c")) {
				
				Connection con = null;
				
				try {
					
					con = MySQLConnectionUtil.getNewConnection();
					CosmeticsAPI.addChest(((Player)cs).getUniqueId(), 1, con);
					
				} catch (SQLException e) {
					e.printStackTrace();
				}
				
				MySQLConnectionUtil.closeConnection(con);
				
				return true;
			}
			if(args[0].equalsIgnoreCase("buy")) {
				CosmeticPlayer cp = CosmeticStorage.getCosmeticPlayer(((Player)cs).getUniqueId());
				
				Connection con = null;
				
				try {
					
					con = MySQLConnectionUtil.getNewConnection();
					cp.buyCosmetic(Cosmetic.fromId(Integer.valueOf(args[1])), false, con);
					
				} catch (SQLException e) {
					e.printStackTrace();
				}
				
				MySQLConnectionUtil.closeConnection(con);
				
				return true;
				
			}
			if(args[0].equalsIgnoreCase("reset")) {
				CosmeticPlayer cp = CosmeticStorage.getCosmeticPlayer(((Player)cs).getUniqueId());
				
				cp.resetCosmetics();
				
				return true;
				
			}
			
		}
		
		return true;
	}

}
