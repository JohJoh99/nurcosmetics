package de.johjoh.nurcosmetics;

import java.util.logging.Logger;

import org.bukkit.plugin.java.JavaPlugin;

public class NurCosmeticsPlugin extends JavaPlugin {
	
	private static Logger log;
	private static NurCosmetics manager = null;
	private static NurCosmeticsPlugin instance;
	
	@Override
	public void onLoad() {
		
		log = this.getLogger();
		instance = this;
		manager = new NurCosmetics(this);
		
	}
	
	@Override
	public void onEnable() {
		
		if(log == null) log = this.getLogger();
		if(instance == null) instance = this;
		if(manager == null) manager = new NurCosmetics(this);
		
		manager.onEnable();
		
		log.info("Plugin enabled!");
		
	}
	
	@Override
	public void onDisable() {
		
		if(manager != null) manager.onDisable();
		
		log.info("Plugin disabled!");
		
		
	}
	
	public static NurCosmetics getManager() {
		return manager;
	}
	
	public static NurCosmeticsPlugin getInstance() {
		return instance;
	}

}
