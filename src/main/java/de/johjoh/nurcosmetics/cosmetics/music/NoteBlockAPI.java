package de.johjoh.nurcosmetics.cosmetics.music;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class NoteBlockAPI {
	
	public static NoteBlockAPI noteBlockAPI;
	public HashMap<String, ArrayList<SongPlayer>> playingSongs = new HashMap<String, ArrayList<SongPlayer>>();
	public HashMap<String, Byte> playerVolume = new HashMap<String, Byte>();
	public boolean disabling = false;
	
	public static boolean isReceivingSong(Player p) {
		return (noteBlockAPI.playingSongs.get(p.getName()) != null)
				&& (!((ArrayList<?>) noteBlockAPI.playingSongs.get(p.getName())).isEmpty());
	}

	public static void stopPlaying(Player p) {
		if(noteBlockAPI.playingSongs.get(p.getName()) == null) {
			return;
		}
		for(SongPlayer s : (ArrayList<SongPlayer>) noteBlockAPI.playingSongs.get(p.getName())) {
			s.removePlayer(p);
		}
	}

	public static void setPlayerVolume(Player p, byte volume) {
		noteBlockAPI.playerVolume.put(p.getName(), Byte.valueOf(volume));
	}

	public static byte getPlayerVolume(Player p) {
		Byte b = (Byte) noteBlockAPI.playerVolume.get(p.getName());
		if (b == null) {
			b = Byte.valueOf((byte) 100);
			noteBlockAPI.playerVolume.put(p.getName(), b);
		}
		return b.byteValue();
	}

	public static void onEnable() {
		noteBlockAPI = new NoteBlockAPI();
	}

	public static void onDisable() {
		if(noteBlockAPI != null)
			noteBlockAPI.disabling = true;
	}

	public boolean isPre1_9() {
		if (Bukkit.getVersion().contains("1.8")) {
			return true;
		}
		if (Bukkit.getVersion().contains("1.7")) {
			return true;
		}
		return false;
	}

}
