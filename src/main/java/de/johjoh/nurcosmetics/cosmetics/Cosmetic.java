package de.johjoh.nurcosmetics.cosmetics;

import java.util.Arrays;

import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import net.nurserver.core.rank.Rank;

public enum Cosmetic {
	
//	MORPH_ZOMBIE(100, Category.MORPHS, true, Rank.PLAYER, ChatColor.DARK_GREEN + "Zombie", 20000, Material.SKULL_ITEM, (byte) 2, EntityType.ZOMBIE),
//	MORPH_VILLAGER(101, Category.MORPHS, true, Rank.PLAYER, ChatColor.GOLD + "Dorfbewohner", 1000, Material.EMERALD, (byte) 0, EntityType.VILLAGER),
	
	PET_WOLF(200, Category.PETS, true, Rank.PLAYER, ChatColor.GRAY + "Wolf", 1000, Material.MONSTER_EGG, (byte) 95, EntityType.WOLF),
	PET_COW(201, Category.PETS, true, Rank.PLAYER, ChatColor.GOLD + "Kuh", 1000, Material.MONSTER_EGG, (byte) 92, EntityType.COW),
	PET_RABBIT(202, Category.PETS, true, Rank.PLAYER, ChatColor.GOLD + "Hase", 1000, Material.MONSTER_EGG, (byte) 101, EntityType.RABBIT),
	PET_SILVERFISH(203, Category.PETS, true, Rank.PLAYER, ChatColor.GRAY + "Silberfisch", 1000, Material.MONSTER_EGG, (byte) 60, EntityType.SILVERFISH),
	PET_CHICKEN(204, Category.PETS, true, Rank.PLAYER, ChatColor.GRAY + "Huhn", 1000, Material.MONSTER_EGG, (byte) 93, EntityType.CHICKEN),
	PET_SHEEP(205, Category.PETS, true, Rank.PLAYER, ChatColor.WHITE + "Schaf", 1000, Material.MONSTER_EGG, (byte) 91, EntityType.SHEEP),
	PET_PIG(206, Category.PETS, true, Rank.PLAYER, ChatColor.LIGHT_PURPLE + "Schwein", 1000, Material.MONSTER_EGG, (byte) 90, EntityType.PIG),
	PET_GUARDIAN(207, Category.PETS, true, Rank.PLAYER, ChatColor.AQUA + "W\u00e4chter", 1000, Material.MONSTER_EGG, (byte) 68, EntityType.GUARDIAN),
	PET_HORSE(208, Category.PETS, true, Rank.PLAYER, ChatColor.GOLD + "Pferd", 1000, Material.MONSTER_EGG, (byte) 100, EntityType.HORSE),
//	PET_BAT(209, Category.PETS, true, Rank.PREMIUM, ChatColor.BLACK + "Fledermaus", 1000, Material.MONSTER_EGG, (byte) 65, EntityType.BAT),
//	PET_ENDERDRAGON(208, Category.PETS, true, Rank.PREMIUM, ChatColor.DARK_PURPLE + "Enderdragon", 1000, Material.MONSTER_EGG, (byte) 58, EntityType.ENDER_DRAGON),
	
	MUSIC_SONG_OF_STORMS(300, Category.MUSIC, true, Rank.PLAYER, ChatColor.DARK_GREEN + "Song of Storms", 1000, Material.RECORD_10, (byte) 0, "Song of Storms"),
	MUSIC_NYAN_CAT(301, Category.MUSIC, true, Rank.PLAYER, ChatColor.LIGHT_PURPLE + "Nyan Cat", 1000, Material.RECORD_7, (byte) 0, "Nyan Cat"),
	MUSIC_A_KIND_OF_MAGIC(302, Category.MUSIC, true, Rank.PLAYER, ChatColor.DARK_PURPLE + "A Kind of Magic", 1000, Material.RECORD_6, (byte) 0, "A Kind of Magic"),
	MUSIC_ALL_STAR(303, Category.MUSIC, true, Rank.PLAYER, ChatColor.DARK_RED + "All Star", 1000, Material.RECORD_4, (byte) 0, "All Star"),
	MUSIC_TETRIS_A_THEME(304, Category.MUSIC, true, Rank.PLAYER, ChatColor.BLUE + "Tetris A Theme", 1000, Material.RECORD_12, (byte) 0, "Tetris A Theme"),
	MUSIC_WHAT_IS_LOVE(305, Category.MUSIC, true, Rank.PLAYER, ChatColor.RED + "What is Love", 1000, Material.RECORD_3, (byte) 0, "What is Love"),
	MUSIC_POKEMON(306, Category.MUSIC, true, Rank.PLAYER, ChatColor.GOLD + "Pokemon Center Theme", 1000, Material.GOLD_RECORD, (byte) 0, "Pokemon Center Theme"),
	MUSIC_JUST_GIVE_ME_A_REASON(307, Category.MUSIC, true, Rank.PLAYER, ChatColor.GOLD + "Just Give Me a Reason", 1000, Material.RECORD_12, (byte) 0, "Just Give Me a Reason"),
	MUSIC_GHOSTBUSTER(308, Category.MUSIC, true, Rank.PLAYER, ChatColor.WHITE + "Ghostbusters", 1000, Material.RECORD_9, (byte) 0, "Ghostbusters"),
	MUSIC_FUR_ELISE(309, Category.MUSIC, true, Rank.PLAYER, ChatColor.BLACK + "F\\u00fcr Elise", 1000, Material.RECORD_11, (byte) 0, "Fur Elise"),
	MUSIC_ZELDA_THEME(310, Category.MUSIC, true, Rank.PLAYER, ChatColor.DARK_GREEN + "Zelda Theme", 1000, Material.RECORD_10, (byte) 0, "Zelda Theme"),
	MUSIC_SUPER_MARIO_BROS_3(311, Category.MUSIC, true, Rank.PLAYER, ChatColor.DARK_RED + "Super Mario Bros 3", 1000, Material.RECORD_4, (byte) 0, "Super Mario Bros 3"),
	MUSIC_DUEL_OF_THE_FATES(312, Category.MUSIC, true, Rank.PLAYER, ChatColor.GOLD + "Duel of the Fates", 1000, Material.RECORD_3, (byte) 0, "Duel of the Fates"),
	
	HAT_GLASS(400, Category.HATS, true, Rank.PLAYER, ChatColor.GRAY + "Glas", 1000, Material.GLASS, (byte) 0, null),
	HAT_TNT(401, Category.HATS, true, Rank.PLAYER, ChatColor.DARK_RED + "TNT", 1000, Material.TNT, (byte) 0, null),
	HAT_SEA_LANTERN(402, Category.HATS, true, Rank.PLAYER, ChatColor.AQUA + "See Laterne", 1000, Material.SEA_LANTERN, (byte) 0, null),
	HAT_GLOWSTONE(403, Category.HATS, true, Rank.PLAYER, ChatColor.YELLOW + "Glowstone", 1000, Material.GLOWSTONE, (byte) 0, null),
	HAT_FENCE(404, Category.HATS, true, Rank.PLAYER, ChatColor.GOLD + "Zaun", 1000, Material.FENCE, (byte) 0, null),
	HAT_COBBLE_WALL(405, Category.HATS, true, Rank.PLAYER, ChatColor.DARK_GRAY + "Steinwand", 1000, Material.COBBLE_WALL, (byte) 0, null),
	HAT_ANVIL(406, Category.HATS, true, Rank.PLAYER, ChatColor.DARK_GRAY + "Amboss", 1000, Material.ANVIL, (byte) 0, null),
	HAT_CHEST(407, Category.HATS, true, Rank.PLAYER, ChatColor.GOLD + "Kiste", 1000, Material.CHEST, (byte) 0, null),
	HAT_WORKBENCH(408, Category.HATS, true, Rank.PLAYER, ChatColor.GOLD + "Werkbank", 1000, Material.WORKBENCH, (byte) 0, null),
	HAT_SLIME_BLOCK(409, Category.HATS, true, Rank.PLAYER, ChatColor.GREEN + "Slimeblock", 1000, Material.SLIME_BLOCK, (byte) 0, null),
	HAT_COMMAND_BLOCK(410, Category.HATS, true, Rank.PLAYER, ChatColor.GOLD + "Commandblock", 1000, Material.COMMAND, (byte) 0, null),
	HAT_NOTE_BLOCK(411, Category.HATS, true, Rank.PLAYER, ChatColor.GOLD + "Noteblock", 1000, Material.NOTE_BLOCK, (byte) 0, null),
	HAT_SKULL_SLIME(420, Category.HATS, true, Rank.PLAYER, ChatColor.GREEN + "Slime", 1000, Material.SKULL_ITEM, (byte) 3, "MHF_Slime"),
	HAT_SKULL_PRESENT(421, Category.HATS, true, Rank.PLAYER, ChatColor.DARK_RED + "Geschenk", 1000, Material.SKULL_ITEM, (byte) 3, "MHF_Present2"),
	HAT_SKULL_LLAMA(422, Category.HATS, true, Rank.PLAYER, ChatColor.WHITE + "Lama", 1000, Material.SKULL_ITEM, (byte) 3, "AlphaLama_"),
	HAT_SKULL_QUESTION_MARK(423, Category.HATS, true, Rank.PLAYER, ChatColor.GOLD + "Fragezeichen", 1000, Material.SKULL_ITEM, (byte) 3, "MHF_Question"),
	HAT_SKULL_BEER(424, Category.HATS, true, Rank.PLAYER, ChatColor.GOLD + "Bier", 1000, Material.SKULL_ITEM, (byte) 3, "Thanauser"),
	HAT_SKULL_ERROR(425, Category.HATS, true, Rank.PLAYER, ChatColor.DARK_RED + "Error", 1000, Material.SKULL_ITEM, (byte) 3, "Agusrodri09"),
	HAT_SKULL_PORTAL_CUBE(426, Category.HATS, true, Rank.PLAYER, ChatColor.LIGHT_PURPLE + "Portal Cube", 1000, Material.SKULL_ITEM, (byte) 3, "Mercury777"),

	EFFECTS_SPEED_1(500, Category.EFFECTS, true, Rank.PLAYER, ChatColor.BLUE + "Speed I", 1000, Material.POTION, (byte) 8194, new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 0)),
	EFFECTS_SPEED_2(501, Category.EFFECTS, true, Rank.PLAYER, ChatColor.BLUE + "Speed II", 1000, Material.POTION, (byte) 8226, new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 1)),
	EFFECTS_JUMP_1(502, Category.EFFECTS, true, Rank.PLAYER, ChatColor.AQUA + "Jump I", 1000, Material.POTION, (byte) 8203, new PotionEffect(PotionEffectType.JUMP, Integer.MAX_VALUE, 0)),
	EFFECTS_JUMP_2(503, Category.EFFECTS, true, Rank.PLAYER, ChatColor.AQUA + "Jump II", 1000, Material.POTION, (byte) 8235, new PotionEffect(PotionEffectType.JUMP, Integer.MAX_VALUE, 1)),
	EFFECTS_NIGHT_VISION(504, Category.EFFECTS, true, Rank.PLAYER, ChatColor.DARK_BLUE + "Nachtsicht", 1000, Material.POTION, (byte) 8230, new PotionEffect(PotionEffectType.NIGHT_VISION, Integer.MAX_VALUE, 0)),

	PARTICLE_TRAIL_WATER(600, Category.PARTICLE_TRAIL, true, Rank.PLAYER, ChatColor.DARK_BLUE + "Wasser", 1000, Material.WATER_BUCKET, (byte) 0, Effect.WATERDRIP),
	PARTICLE_TRAIL_COLORED_DUST(601, Category.PARTICLE_TRAIL, true, Rank.PLAYER, ChatColor.DARK_RED + "Farbe", 1000, Material.INK_SACK, (byte) 1, Effect.COLOURED_DUST),
	PARTICLE_TRAIL_HEART(602, Category.PARTICLE_TRAIL, true, Rank.PLAYER, ChatColor.DARK_RED + "Herz", 1000, Material.RED_ROSE, (byte) 0, Effect.HEART),
	PARTICLE_TRAIL_FIRE(603, Category.PARTICLE_TRAIL, true, Rank.PLAYER, ChatColor.GOLD + "Feuer", 1000, Material.FLINT_AND_STEEL, (byte) 0, Effect.LAVA_POP),
	PARTICLE_TRAIL_LAVA(604, Category.PARTICLE_TRAIL, true, Rank.PLAYER, ChatColor.GOLD + "Lava", 1000, Material.LAVA_BUCKET, (byte) 0, Effect.LAVADRIP),
	PARTICLE_TRAIL_NOTE(605, Category.PARTICLE_TRAIL, true, Rank.PLAYER, ChatColor.YELLOW + "Musiknote", 1000, Material.NOTE_BLOCK, (byte) 0, Effect.NOTE),
	PARTICLE_TRAIL_HAPPY_VILLAGER(606, Category.PARTICLE_TRAIL, true, Rank.PLAYER, ChatColor.GREEN + "Emerald", 1000, Material.EMERALD, (byte) 0, Effect.HAPPY_VILLAGER),
	PARTICLE_TRAIL_CHARGED_CLOUD(607, Category.PARTICLE_TRAIL, true, Rank.PLAYER, ChatColor.GRAY + "Charged Cloud", 1000, Material.FIREBALL, (byte) 0, Effect.VILLAGER_THUNDERCLOUD),
	PARTICLE_TRAIL_CLOUD(608, Category.PARTICLE_TRAIL, true, Rank.PLAYER, ChatColor.WHITE + "Cloud", 1000, Material.FIREBALL, (byte) 0, Effect.CLOUD),

	GADGETS_GRAPLING_HOOK(700, Category.GADGETS, true, Rank.PLAYER, ChatColor.AQUA + "Enterhaken", 1000, Material.FISHING_ROD, (byte) 0, null),
	GADGETS_ENDER_PEARL(701, Category.GADGETS, true, Rank.PLAYER, ChatColor.DARK_PURPLE + "Enderperle", 1000, Material.ENDER_PEARL, (byte) 0, null);
//	GADGETS_BOAT(702, Category.GADGETS, true, Rank.PLAYER, ChatColor.GOLD + "Boat", 1000, Material.BOAT, (byte) 0, null);
	
	private int id;
	private Category category;
	private boolean soloUse;
	private Rank minRank;
	private String displayName;
	private int price;
	private Material item;
	private byte data;
	private Object modifier;
	
	Cosmetic(int id, Category category, boolean soloUse, Rank minRank, String displayName, int price, Material item, byte data, Object modifier) {
		this.id = id;
		this.category = category;
		this.soloUse = soloUse;
		this.minRank = minRank;
		this.displayName = displayName;
		this.price = price;
		this.item = item;
		this.data = data;
		this.modifier = modifier;
	}
	
	public int getId() {
		return this.id;
	}
	
	public Category getCategory() {
		return this.category;
	}
	
	public boolean isSoloUse() {
		return this.soloUse;
	}
	
	public Rank getMinRank() {
		return this.minRank;
	}
	
	public String getDisplayName() {
		return this.displayName;
	}
	
	public int getPrice() {
		return this.price;
	}
	
	public Material getItem() {
		return this.item;
	}
	
	public byte getData() {
		return this.data;
	}
	
	public Object getModifier() {
		return this.modifier;
	}
	
	public ItemStack getItemStack(boolean clear, boolean owned, boolean buyable, boolean ctb) {
		String lore = ChatColor.DARK_GRAY + "� " + (owned ? ChatColor.GREEN + "In Besitz" : ChatColor.YELLOW + "Preis" + ChatColor.GRAY + ": " + (buyable ? ChatColor.GREEN : ChatColor.RED) + price) + ChatColor.DARK_GRAY + " �";
		ItemStack is = new ItemStack(item, 1, data);
		if(category == Category.HATS && data == 3) {
			SkullMeta im = (SkullMeta)is.getItemMeta();
			im.setDisplayName(displayName);
			im.setOwner((String)modifier);
			if(!clear)
				im.setLore(Arrays.asList(new String[]{lore, ((!owned && buyable && ctb) ? ChatColor.LIGHT_PURPLE + "Klicke zum kaufen!" : "")}));
			is.setItemMeta(im);
		}
		else {
			ItemMeta im = is.getItemMeta();
			im.setDisplayName(displayName);
			if(!clear)
				im.setLore(Arrays.asList(new String[]{lore, ((!owned && buyable && ctb) ? ChatColor.LIGHT_PURPLE + "Klicke zum kaufen!" : "")}));
			is.setItemMeta(im);
		}
		return is;
	}
	
	public static Cosmetic fromId(int id) {
		for(Cosmetic c : Cosmetic.values()) {
			if(c.getId() == id) return c;
		}
		
		return null;
		
	}
	
	public static Cosmetic fromDisplayName(String displayName) {
		for(Cosmetic c : Cosmetic.values()) {
			if(c.getDisplayName().equalsIgnoreCase(displayName))
				return c;
		}
		
		return null;
		
	}
	
}
