package de.johjoh.nurcosmetics.cosmetics;

import org.bukkit.ChatColor;
import org.bukkit.Material;

public enum Category {
	
//	MORPHS("Morph", ChatColor.DARK_PURPLE + "Morph", Material.RAW_FISH, (byte) 0),
	PETS("Haustier", ChatColor.GOLD + "Haustiere", Material.MONSTER_EGG, (byte) 91),
	MUSIC("Musik", ChatColor.DARK_RED + "Musik", Material.JUKEBOX, (byte) 0),
	HATS("Hut", ChatColor.GREEN + "H\u00fcte", Material.GLASS, (byte) 0),
	EFFECTS("Effekt", ChatColor.LIGHT_PURPLE + "Effekte", Material.POTION, (byte) 8194),
	PARTICLE_TRAIL("Partikelspur", ChatColor.DARK_RED + "Partikelspur", Material.INK_SACK, (byte) 1),
//	CLOTHES("Garderobe", ChatColor.GOLD + "Garderobe", Material.GOLD_HELMET, (byte) 0),
	GADGETS("Gadgets", ChatColor.AQUA + "Gadgets", Material.FISHING_ROD, (byte) 0);
	
	private String name;
	private String displayName;
	private Material item;
	private byte data;

	private Category(String name, String displayName, Material item, byte data) {
		this.name = name;
		this.displayName = displayName;
		this.item = item;
		this.data = data;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getDisplayName() {
		return this.displayName;
	}
	
	public Material getMaterial() {
		return this.item;
	}
	
	public byte getData() {
		return data;
	}
	
	public static Category fromDisplayName(String displayName) {
		for(Category c : Category.values()) {
			if(c.getDisplayName().equalsIgnoreCase(displayName))
				return c;
		}
		
		return null;
		
	}

}
