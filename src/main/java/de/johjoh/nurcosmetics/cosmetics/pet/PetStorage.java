package de.johjoh.nurcosmetics.cosmetics.pet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

import org.bukkit.entity.Entity;

import de.johjoh.nurcosmetics.cosmetics.pet.types.BatEntityPet;
import de.johjoh.nurcosmetics.cosmetics.pet.types.ChickenEntityPet;
import de.johjoh.nurcosmetics.cosmetics.pet.types.CowEntityPet;
import de.johjoh.nurcosmetics.cosmetics.pet.types.EnderDragonEntityPet;
import de.johjoh.nurcosmetics.cosmetics.pet.types.GuardianEntityPet;
import de.johjoh.nurcosmetics.cosmetics.pet.types.HorseEntityPet;
import de.johjoh.nurcosmetics.cosmetics.pet.types.PigEntityPet;
import de.johjoh.nurcosmetics.cosmetics.pet.types.RabbitEntityPet;
import de.johjoh.nurcosmetics.cosmetics.pet.types.SheepEntityPet;
import de.johjoh.nurcosmetics.cosmetics.pet.types.SilverfishEntityPet;
import de.johjoh.nurcosmetics.cosmetics.pet.types.WolfEntityPet;
import net.minecraft.server.v1_8_R3.EntityBat;
import net.minecraft.server.v1_8_R3.EntityChicken;
import net.minecraft.server.v1_8_R3.EntityCow;
import net.minecraft.server.v1_8_R3.EntityEnderDragon;
import net.minecraft.server.v1_8_R3.EntityGuardian;
import net.minecraft.server.v1_8_R3.EntityHorse;
import net.minecraft.server.v1_8_R3.EntityPig;
import net.minecraft.server.v1_8_R3.EntityRabbit;
import net.minecraft.server.v1_8_R3.EntitySheep;
import net.minecraft.server.v1_8_R3.EntitySilverfish;
import net.minecraft.server.v1_8_R3.EntityWolf;

public class PetStorage {
	
	private static HashMap<UUID, Pet> pets = new HashMap<UUID, Pet>();
	
	public static Pet getPet(UUID uniqueId) {
		return pets.get(uniqueId);
	}
	
	public static HashMap<UUID, Pet> getPets() {
		return pets;
	}
	
	public static ArrayList<Entity> getEntitys() {
		ArrayList<Entity> e = new ArrayList<Entity>();
		for(Pet p : pets.values()) {
			e.add(p.getEntity());
		}
		
		return e;
	}
	
	public static void addPet(UUID uniqueId, Pet pet) {
		pets.put(uniqueId, pet);
	}
	
	public static void removePet(UUID uniqueId) {
		pets.remove(uniqueId);
	}

	public static void r() {
		NMSUtils.registerEntity("Bat", 65, EntityBat.class, BatEntityPet.class);
		NMSUtils.registerEntity("Chicken", 93, EntityChicken.class, ChickenEntityPet.class);
		NMSUtils.registerEntity("Cow", 92, EntityCow.class, CowEntityPet.class);
		NMSUtils.registerEntity("EnderDragon", 63, EntityEnderDragon.class, EnderDragonEntityPet.class);
		NMSUtils.registerEntity("Guardian", 68, EntityGuardian.class, GuardianEntityPet.class);
		NMSUtils.registerEntity("Horse", 100, EntityHorse.class, HorseEntityPet.class);
		NMSUtils.registerEntity("Pig", 90, EntityPig.class, PigEntityPet.class);
		NMSUtils.registerEntity("Rabbit", 101, EntityRabbit.class, RabbitEntityPet.class);
		NMSUtils.registerEntity("Sheep", 91, EntitySheep.class, SheepEntityPet.class);
		NMSUtils.registerEntity("Silverfish", 60, EntitySilverfish.class, SilverfishEntityPet.class);
		NMSUtils.registerEntity("Wolf", 95, EntityWolf.class, WolfEntityPet.class);
	}

}
