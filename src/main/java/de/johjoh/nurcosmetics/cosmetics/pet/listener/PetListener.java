package de.johjoh.nurcosmetics.cosmetics.pet.listener;

import org.bukkit.ChatColor;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;

import de.johjoh.nurcosmetics.cosmetics.pet.PetStorage;
import de.johjoh.nurcosmetics.util.CosmeticPlayer;
import de.johjoh.nurcosmetics.util.CosmeticStorage;

public class PetListener implements Listener {
	
	@EventHandler
	public void onEntityDamage(EntityDamageEvent e) {
		if(PetStorage.getEntitys().contains(e.getEntity())) {
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onPlayerInteractAtEntity(PlayerInteractAtEntityEvent e) {
		if(e.getRightClicked() instanceof LivingEntity) {
			CosmeticPlayer cp = CosmeticStorage.getCosmeticPlayer(e.getPlayer().getUniqueId());
			if(cp != null && cp.getPet() != null && cp.getPet().getEntity() != null && cp.getPet().getEntity() == e.getRightClicked()) {
				e.setCancelled(true);
				e.getPlayer().openInventory(cp.getPet().getSettingInventory());
			}
		}
	}
	
	@EventHandler
	public void onInventoryClick(InventoryClickEvent e) {
		if(e.getInventory().getTitle().equalsIgnoreCase(ChatColor.GOLD + "Haustier" + ChatColor.DARK_GRAY + "-" + ChatColor.GOLD + "Einstellungen")) {
			e.setCancelled(true);
			if(e.getCurrentItem() == null || !e.getCurrentItem().hasItemMeta()) return;
		}
	}

}
