package de.johjoh.nurcosmetics.cosmetics.pet;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import de.johjoh.nurcosmetics.NurCosmeticsPlugin;
import net.minecraft.server.v1_8_R3.AttributeInstance;
import net.minecraft.server.v1_8_R3.EntityInsentient;
import net.minecraft.server.v1_8_R3.GenericAttributes;
import net.minecraft.server.v1_8_R3.PathEntity;

public class FollowUtil {
	
	public static void petFollow(final Player player, final Entity pet) {
		petFollow(player, pet, 0.3);
	}
	
	public static void petFollow(final Player player, final Entity pet, final double speed) {
		new BukkitRunnable() {
			@SuppressWarnings("deprecation")
			public void run() {
				if(pet == null || !pet.isValid() || player == null || (!player.isOnline())) {
					this.cancel();
					return;
				}
				net.minecraft.server.v1_8_R3.Entity pett = ((CraftEntity) pet).getHandle();
				((EntityInsentient) pett).getNavigation().a(2);
				Object petf = ((CraftEntity) pet).getHandle();
				Location targetLocation = player.getLocation();
				PathEntity path;
				path = ((EntityInsentient) petf).getNavigation().a(targetLocation.getX() + 1, targetLocation.getY(), targetLocation.getZ() + 1);
				if(path != null) {
					((EntityInsentient) petf).getNavigation().a(path, 1.0D);
					((EntityInsentient) petf).getNavigation().a(2.0D);
				}
				int distance = (int) Bukkit.getPlayer(player.getName()).getLocation().distance(pet.getLocation());
				if(distance > 10 && !pet.isDead() && player.isOnGround()) {
					pet.teleport(player.getLocation());
				}
				AttributeInstance attributes = ((EntityInsentient) ((CraftEntity) pet).getHandle())
						.getAttributeInstance(GenericAttributes.MOVEMENT_SPEED);
//						.getAttributeInstance(GenericAttributes.d);
				attributes.setValue(speed);
			}
		}.runTaskTimer(NurCosmeticsPlugin.getInstance(), 0L, 20L);
	}
	
}
