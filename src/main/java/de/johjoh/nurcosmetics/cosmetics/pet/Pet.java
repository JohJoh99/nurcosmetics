package de.johjoh.nurcosmetics.cosmetics.pet;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Ageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Pig;
import org.bukkit.entity.Player;
import org.bukkit.entity.Vehicle;
import org.bukkit.entity.Wolf;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import de.johjoh.nurcosmetics.NurCosmetics;
import de.johjoh.nurcosmetics.NurCosmeticsPlugin;
import de.johjoh.nurcosmetics.cosmetics.Cosmetic;
import de.johjoh.nurcosmetics.cosmetics.pet.types.BatEntityPet;
import de.johjoh.nurcosmetics.cosmetics.pet.types.ChickenEntityPet;
import de.johjoh.nurcosmetics.cosmetics.pet.types.CowEntityPet;
import de.johjoh.nurcosmetics.cosmetics.pet.types.EnderDragonEntityPet;
import de.johjoh.nurcosmetics.cosmetics.pet.types.GuardianEntityPet;
import de.johjoh.nurcosmetics.cosmetics.pet.types.HorseEntityPet;
import de.johjoh.nurcosmetics.cosmetics.pet.types.PigEntityPet;
import de.johjoh.nurcosmetics.cosmetics.pet.types.RabbitEntityPet;
import de.johjoh.nurcosmetics.cosmetics.pet.types.SheepEntityPet;
import de.johjoh.nurcosmetics.cosmetics.pet.types.SilverfishEntityPet;
import de.johjoh.nurcosmetics.cosmetics.pet.types.WolfEntityPet;
import de.johjoh.nurcosmetics.util.AnvilGUI;
import de.johjoh.nurcosmetics.util.AnvilGUI.AnvilClickEvent;
import de.johjoh.nurcosmetics.util.CosmeticPlayer;
import de.johjoh.nurcosmetics.util.item.InteractableItem;
import de.johjoh.nurcosmetics.util.item.ItemCreator;
import de.johjoh.nurcosmetics.util.item.PlayerRunnable;

public class Pet {
	
	private Cosmetic type;
	private Player owner;
	private boolean spawned = false;
	private boolean riding = false;
	private boolean baby = false;
	private boolean saddle = false;
	private EntityType entityType;
	private Entity entity;
	private String name;
	private Inventory settingInventory;
	
	public Pet(Cosmetic type, Player owner) {
		this.type = type;
		this.owner = owner;
		entityType = (EntityType) type.getModifier();
		name = owner.getDisplayName() + "'s Haustier";
		settingInventory = Bukkit.createInventory(null, 9, ChatColor.GOLD + "Haustier" + ChatColor.DARK_GRAY + "-" + ChatColor.GOLD + "Einstellungen");
		
		ItemStack gs = new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte) 15); ItemMeta gm = gs.getItemMeta(); gm.setDisplayName(" "); gs.setItemMeta(gm);
		for(int it = 0; it < 9; it++) settingInventory.setItem(it, gs);
		
	}
	
	public Cosmetic getType() {
		return this.type;
	}
	
	public Player getOwner() {
		return this.owner;
	}
	
	public boolean isSpawned() {
		return this.spawned;
	}
	
	public boolean isRiding() {
		return this.riding;
	}
	
	public boolean isBaby() {
		return this.baby;
	}
	
	public boolean isSaddeled() {
		return this.saddle;
	}
	
	public EntityType getEntityType() {
		return this.entityType;
	}
	
	public Entity getEntity() {
		return this.entity;
	}
	
	public String getName() {
		return this.name;
	}
	
	public Inventory getSettingInventory() {
		return this.settingInventory;
	}
	
	public void setName(String name) {
		this.name = name;
		entity.setCustomName(name.replace("&", "�"));
		updateInventory();
	}
	
	public void setBaby(boolean baby) {
		if(baby) {
			if(entity instanceof Ageable) {
				((Ageable)entity).setBaby();
				this.baby = true;
			}
		}
		else {
			if(entity instanceof Ageable) {
				((Ageable)entity).setAdult();
				this.baby = false;
			}
		}
		updateInventory();
	}
	
	public boolean isAgeable() {
		return (entity instanceof Ageable);
	}
	
	public boolean isColorable() {
		return (entity instanceof Wolf);
	}
	
	public boolean saddle() {
		return (entity instanceof Vehicle);
	}
	
	public void rideOwner() {
		owner.setPassenger(entity);
		riding = true;
		updateInventory();
	}
	
	public void stopRiding() {
		owner.eject();
		riding = false;
		updateInventory();
	}
	
	public void spawn(Location loc) {
		
		if(entityType == EntityType.BAT) {
			entity = BatEntityPet.spawn(loc);
		}
		else if(entityType == EntityType.CHICKEN) {
			entity = ChickenEntityPet.spawn(loc);
		}
		else if(entityType == EntityType.COW) {
			entity = CowEntityPet.spawn(loc);
		}
		else if(entityType == EntityType.ENDER_DRAGON) {
			entity = EnderDragonEntityPet.spawn(loc);
		}
		else if(entityType == EntityType.GUARDIAN) {
			entity = GuardianEntityPet.spawn(loc);
		}
		else if(entityType == EntityType.HORSE) {
			entity = HorseEntityPet.spawn(loc);
			((Horse)entity).setTamed(true);
		}
		else if(entityType == EntityType.PIG) {
			entity = PigEntityPet.spawn(loc);
		}
		else if(entityType == EntityType.RABBIT) {
			entity = RabbitEntityPet.spawn(loc);
		}
		else if(entityType == EntityType.SHEEP) {
			entity = SheepEntityPet.spawn(loc);
		}
		else if(entityType == EntityType.SILVERFISH) {
			entity = SilverfishEntityPet.spawn(loc);
		}
		else if(entityType == EntityType.WOLF) {
			entity = WolfEntityPet.spawn(loc);
		}
		
		setName(name);
		entity.setCustomNameVisible(true);
		FollowUtil.petFollow(owner, entity);
		
		
		spawned = true;
	}
	
	public void despawn() {
		
		if(entity != null)
			entity.remove();
		entity = null;
		
		PetStorage.removePet(owner.getUniqueId());
		
		spawned = false;
	}
	
	@SuppressWarnings("deprecation")
	private void updateInventory() {
		
		InteractableItem name = new InteractableItem(owner.getUniqueId(), Material.NAME_TAG, ChatColor.GREEN + "Name \u00e4ndern", new PlayerRunnable() {
			public void run(final CosmeticPlayer player) {
				AnvilGUI ag = new AnvilGUI(player.getPlayer(), new AnvilGUI.AnvilClickEventHandler() {
					public void onAnvilClick(AnvilClickEvent event) {
						if(event.getCurrentItem() == null || !event.getCurrentItem().hasItemMeta()) {
							event.setWillClose(false);
							event.setWillDestroy(false);
							return;
						}
						if(event.getSlot() == AnvilGUI.AnvilSlot.OUTPUT) {
							event.setWillClose(true);
							event.setWillDestroy(true);
							setName(event.getName());
							player.getPlayer().sendMessage(NurCosmetics.PREFIX + ChatColor.GRAY + "Dein Haustier hei\u00dft nun " + ChatColor.YELLOW + event.getName());
						} else {
							event.setWillClose(false);
							event.setWillDestroy(false);
						}
					}
				});
				ag.setSlot(AnvilGUI.AnvilSlot.INPUT_LEFT, ItemCreator.namedItem(Material.NAME_TAG, Pet.this.name));
				ag.open();
			}
		});
		settingInventory.setItem(0, name);
		
		
		InteractableItem ride = new InteractableItem(owner.getUniqueId(), Material.SADDLE, ChatColor.YELLOW + "Reiten", new PlayerRunnable() {
			public void run(CosmeticPlayer player) {
				stopRiding();
				player.getPlayer().playSound(player.getPlayer().getEyeLocation(), Sound.CLICK, 1.0F, 1.0F);
				player.getPlayer().closeInventory();
				
				Bukkit.getScheduler().runTaskLater(NurCosmeticsPlugin.getInstance(), new Runnable() {
					public void run() {
						entity.setPassenger(owner);
					}
				}, 5L);
			}
		});
		settingInventory.setItem(1, ride);
		
		
		if(!riding) {
			InteractableItem rideOwner = new InteractableItem(owner.getUniqueId(), Material.GOLD_HELMET, ChatColor.RED + "Als Hut setzen", new PlayerRunnable() {
				public void run(CosmeticPlayer player) {
					player.getPlayer().playSound(player.getPlayer().getEyeLocation(), Sound.CLICK, 1.0F, 1.0F);
					rideOwner();
					player.getPlayer().closeInventory();
					player.getPlayer().sendMessage(NurCosmetics.PREFIX + ChatColor.GREEN + "Du tr\u00e4gst dein Haustier nun auf deinem Kopf");
				}
			});
			settingInventory.setItem(2, rideOwner);
		}
		else {
			InteractableItem rideOwner = new InteractableItem(owner.getUniqueId(), Material.GOLD_HELMET, ChatColor.RED + "Von Kopf entfernen", new PlayerRunnable() {
				public void run(CosmeticPlayer player) {
					player.getPlayer().playSound(player.getPlayer().getEyeLocation(), Sound.CLICK, 1.0F, 1.0F);
					stopRiding();
					player.getPlayer().closeInventory();
					player.getPlayer().sendMessage(NurCosmetics.PREFIX + ChatColor.RED + "Du hast dein Haustier abgesetzt");
				}
			});
			settingInventory.setItem(2, rideOwner);
		}
		
		if(isAgeable()) {
			if(!baby) {
				InteractableItem adult = new InteractableItem(owner.getUniqueId(), Material.EGG, ChatColor.GREEN + "Erwachsen", new PlayerRunnable() {
					public void run(CosmeticPlayer player) {
						player.getPlayer().playSound(player.getPlayer().getEyeLocation(), Sound.CLICK, 1.0F, 1.0F);
						setBaby(true);
						player.getPlayer().closeInventory();
						player.getPlayer().sendMessage(NurCosmetics.PREFIX + ChatColor.GREEN + "Dein Haustier ist nun ein Baby");
					}
				});
				settingInventory.setItem(3, adult);
			}
			else {
				InteractableItem baby = new InteractableItem(owner.getUniqueId(), Material.EGG, ChatColor.GREEN + "Baby", new PlayerRunnable() {
					public void run(CosmeticPlayer player) {
						player.getPlayer().playSound(player.getPlayer().getEyeLocation(), Sound.CLICK, 1.0F, 1.0F);
						setBaby(false);
						player.getPlayer().closeInventory();
						player.getPlayer().sendMessage(NurCosmetics.PREFIX + ChatColor.GREEN + "Dein Haustier ist nun Erwachsen");
					}
				});
				settingInventory.setItem(3, baby);
			}
		}
		
		if(saddle()) {
			if(!isSaddeled()) {
				InteractableItem s = new InteractableItem(owner.getUniqueId(), Material.SADDLE, ChatColor.YELLOW + "Sattel aufsetzen", new PlayerRunnable() {
					public void run(CosmeticPlayer player) {
						player.getPlayer().playSound(player.getPlayer().getEyeLocation(), Sound.CLICK, 1.0F, 1.0F);
						if(entityType == EntityType.HORSE)
							((Horse)entity).getInventory().setSaddle(ItemCreator.namedItem(Material.SADDLE, ChatColor.GOLD + "Sattel"));
						if(entityType == EntityType.PIG)
							((Pig)entity).setSaddle(true);
						saddle = true;
						player.getPlayer().closeInventory();
						updateInventory();
					}
				});
				settingInventory.setItem(4, s);
			}
			else {
				InteractableItem s = new InteractableItem(owner.getUniqueId(), Material.SADDLE, ChatColor.YELLOW + "Sattel absetzen", new PlayerRunnable() {
					public void run(CosmeticPlayer player) {
						player.getPlayer().playSound(player.getPlayer().getEyeLocation(), Sound.CLICK, 1.0F, 1.0F);
						if(entityType == EntityType.HORSE)
							((Horse)entity).getInventory().setSaddle(null);
						if(entityType == EntityType.PIG)
							((Pig)entity).setSaddle(false);
						saddle = false;
						player.getPlayer().closeInventory();
						updateInventory();
					}
				});
				settingInventory.setItem(4, s);
			}
		}
		
		if(isColorable()) {
			((Wolf)entity).getCollarColor().getData();
		}
		
		
		InteractableItem close = new InteractableItem(owner.getUniqueId(), Material.BARRIER, ChatColor.RED + "Schie\u00dfen", new PlayerRunnable() {
			public void run(CosmeticPlayer player) {
				player.getPlayer().playSound(player.getPlayer().getEyeLocation(), Sound.CLICK, 1.0F, 1.0F);
				owner.closeInventory();
			}
		});
		settingInventory.setItem(8, close);
	}

}
