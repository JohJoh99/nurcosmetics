package de.johjoh.nurcosmetics.cosmetics.effect;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

public class EffectListener implements Listener {
	
	private static HashMap<UUID, Effect> particles = new HashMap<UUID, Effect>();
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onPlayerMove(PlayerMoveEvent e) {
		if(!particles.containsKey(e.getPlayer().getUniqueId())) return;
		for(Player p : Bukkit.getOnlinePlayers())
			p.playEffect(e.getFrom().add(0, 0.1, 0), particles.get(e.getPlayer().getUniqueId()), 1);
	}
	
	public static void registerParticle(UUID uniqueId, Effect effect) {
		particles.put(uniqueId, effect);
	}
	
	public static void unregisterParticle(UUID uniqueId) {
		particles.remove(uniqueId);
	}

}
