package de.johjoh.nurcosmetics.cosmetics.gadget;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.EnderPearl;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.event.player.PlayerFishEvent.State;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import de.johjoh.nurcosmetics.NurCosmeticsPlugin;
import de.johjoh.nurcosmetics.cosmetics.Cosmetic;
import de.johjoh.nurcosmetics.util.item.NamedItem;

public class GadgetListener implements Listener {
	
//	private ArrayList<UUID> boatPlacer = new ArrayList<UUID>();
//	private HashMap<UUID, Entity> boats = new HashMap<UUID, Entity>();
//	
//	@EventHandler
//	public void onEntitySpawn(VehicleCreateEvent e) {
//		Bukkit.broadcastMessage("-1");
//		if(e.getVehicle() instanceof Boat) {
//			Bukkit.broadcastMessage("0");
//			if(boatPlacer.size() > 0) {
//				Bukkit.broadcastMessage("1");
//				Player p = Bukkit.getPlayer(boatPlacer.get(boatPlacer.size()-1));
//				boatPlacer.remove(p.getUniqueId());
//				boats.put(p.getUniqueId(), e.getVehicle());
//				((Boat)e.getVehicle()).setPassenger(p);
//			}
//		}
//	}
//	
//	@EventHandler
//	public void onVehicleExit(VehicleExitEvent e) {
//		if(e.getVehicle() instanceof Boat) {
//			e.getVehicle().remove();
//		}
//	}
	
	@EventHandler
	public void onPlayerInteract(final PlayerInteractEvent e) {
		if(e.getItem() == null || !e.getItem().hasItemMeta() || e.getAction() == Action.LEFT_CLICK_BLOCK || e.getAction() == Action.LEFT_CLICK_AIR) return;
//		if(e.getItem().getItemMeta().getDisplayName().equalsIgnoreCase(Cosmetic.GADGETS_BOAT.getDisplayName())) {
//			if(e.getAction() == Action.RIGHT_CLICK_BLOCK) {
//				if(e.getClickedBlock().getType() == Material.WATER || e.getClickedBlock().getType() == Material.STATIONARY_WATER) {
//					boatPlacer.add(e.getPlayer().getUniqueId());
//					Bukkit.getScheduler().runTaskLater(NurCosmeticsPlugin.getInstance(), new Runnable() {
//						@Override
//						public void run() {
//							e.getPlayer().getInventory().setItem(6, new NamedItem(Cosmetic.GADGETS_BOAT.getItem(), Cosmetic.GADGETS_BOAT.getDisplayName()));
//						}
//					}, 5);
//				}
//			}
//		}
		if(e.getItem().getItemMeta().getDisplayName().equalsIgnoreCase(Cosmetic.GADGETS_ENDER_PEARL.getDisplayName())) {
			e.setCancelled(true);
			e.getPlayer().launchProjectile(EnderPearl.class);
			e.getPlayer().getInventory().setItem(6, new NamedItem(Material.MAGMA_CREAM, " ", ChatColor.DARK_GRAY + "Warte 5 Sekunden.."));
			Bukkit.getScheduler().runTaskLater(NurCosmeticsPlugin.getInstance(), new Runnable() {
				public void run() {
					e.getPlayer().getInventory().setItem(6, new NamedItem(Cosmetic.GADGETS_ENDER_PEARL.getItem(),
							Cosmetic.GADGETS_ENDER_PEARL.getDisplayName()));
				}
			}, 100);
		}
		else if(e.getItem().getItemMeta().getDisplayName().equalsIgnoreCase(Cosmetic.GADGETS_GRAPLING_HOOK.getDisplayName())) {
			e.setCancelled(false);
		}
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void onPlayerGrapple(PlayerGrappleEvent e) {
		if(e.isCancelled()) {
			return;
		}
		Player p = e.getPlayer();
		
		Entity en = e.getPulledEntity();
		Location loc = e.getPullLocation();
		
		if(p.equals(en)) {
			if(p.getLocation().distance(loc) < 3.0D) {
				pullPlayerSlightly(p, loc);
			} else {
				pullEntityToLocation(p, loc);
			}
		} else {
			pullEntityToLocation(en, loc);
			if((e instanceof Item)) {
				ItemStack is = ((Item) e).getItemStack();
				String itemName = is.getType().toString().replace("_", " ").toLowerCase();
				p.sendMessage(ChatColor.GOLD + "You have hooked a stack of " + is.getAmount() + " " + itemName + "!");
			}
		}
		p.getWorld().playSound(p.getEyeLocation(), Sound.MAGMACUBE_JUMP, 10.0F, 1.0F);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerFish(PlayerFishEvent e) {
		Player p = e.getPlayer();
		if(p.getItemInHand() == null || !p.getItemInHand().hasItemMeta() || !p.getItemInHand().getItemMeta().getDisplayName().equalsIgnoreCase(Cosmetic.GADGETS_GRAPLING_HOOK.getDisplayName())) return;
		
		if(e.getState() == PlayerFishEvent.State.IN_GROUND) {
			
			Location loc = e.getHook().getLocation();
			PlayerGrappleEvent ev = new PlayerGrappleEvent(p, p, loc);
			NurCosmeticsPlugin.getInstance().getServer().getPluginManager().callEvent(ev);
		} else if(e.getState() == State.FAILED_ATTEMPT) {
			if(!blockNear(e.getHook().getLocation().getBlock())) return;
			
			Location loc = e.getHook().getLocation();
			PlayerGrappleEvent ev = new PlayerGrappleEvent(p, p, loc);
			NurCosmeticsPlugin.getInstance().getServer().getPluginManager().callEvent(ev);
			
		}
	}
	
	private void pullPlayerSlightly(Player p, Location loc) {
		if (loc.getY() > p.getLocation().getY()) {
			p.setVelocity(new Vector(0.0D, 0.25D, 0.0D));
			return;
		}
		Location playerLoc = p.getLocation();

		Vector vector = loc.toVector().subtract(playerLoc.toVector());
		p.setVelocity(vector);
	}
	
	
	
	
	
	private void pullEntityToLocation(Entity e, Location loc) {
		Location entityLoc = e.getLocation();
		
		entityLoc.setY(entityLoc.getY() + 0.5D);
		e.teleport(entityLoc);
		
		double g = -0.08D;
		double d = loc.distance(entityLoc);
		double t = d;
		double v_x = (1.0D + 0.07D * t) * (loc.getX() - entityLoc.getX()) / t;
		double v_y = (1.0D + 0.03D * t) * (loc.getY() - entityLoc.getY()) / t - 0.5D * g * t;
		double v_z = (1.0D + 0.07D * t) * (loc.getZ() - entityLoc.getZ()) / t;
		
		Vector v = e.getVelocity();
		v.setX(v_x);
		v.setY(v_y);
		v.setZ(v_z);
		e.setVelocity(v);
	}
	
	private boolean blockNear(Block block) {
		if(block.getRelative(BlockFace.DOWN).getType() != Material.AIR) return true;
		if(block.getRelative(BlockFace.UP).getType() != Material.AIR) return true;
		if(block.getRelative(BlockFace.NORTH).getType() != Material.AIR) return true;
		if(block.getRelative(BlockFace.EAST).getType() != Material.AIR) return true;
		if(block.getRelative(BlockFace.SOUTH).getType() != Material.AIR) return true;
		if(block.getRelative(BlockFace.WEST).getType() != Material.AIR) return true;
		if(block.getRelative(BlockFace.NORTH_EAST).getType() != Material.AIR) return true;
		if(block.getRelative(BlockFace.NORTH_WEST).getType() != Material.AIR) return true;
		if(block.getRelative(BlockFace.SOUTH_EAST).getType() != Material.AIR) return true;
		if(block.getRelative(BlockFace.SOUTH_WEST).getType() != Material.AIR) return true;
		
		return false;
	}
	
}
