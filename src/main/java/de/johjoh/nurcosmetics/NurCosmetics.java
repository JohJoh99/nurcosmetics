package de.johjoh.nurcosmetics;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import de.johjoh.nurcosmetics.cmd.CosmeticsCmd;
import de.johjoh.nurcosmetics.cosmetics.effect.EffectListener;
import de.johjoh.nurcosmetics.cosmetics.gadget.GadgetListener;
import de.johjoh.nurcosmetics.cosmetics.music.NoteBlockAPI;
import de.johjoh.nurcosmetics.cosmetics.pet.PetStorage;
import de.johjoh.nurcosmetics.cosmetics.pet.listener.PetListener;
import de.johjoh.nurcosmetics.listener.GardrobeListener;
import de.johjoh.nurcosmetics.listener.PlayerListener;
import de.johjoh.nurcosmetics.sql.CreateTables;
import de.johjoh.nurcosmetics.sql.MySQLConnectionUtil;
import de.johjoh.nurcosmetics.util.CosmeticStorage;
import de.johjoh.nurcosmetics.util.item.PlayerItemListener;
import net.nurserver.core.database.Database;

public class NurCosmetics {
	
	public static final String ITEM_NAME = ChatColor.GOLD + "Cosmetics";
	
	public static final String TABLE_COSMETIC = "cosmetic";
	public static final String PREFIX = ChatColor.WHITE + "[" + ChatColor.GOLD + "Cosmetics" + ChatColor.WHITE + "] ";
	public static final String COLUMN_ID = "id";
	public static final String COLUMN_PLAYER_UUID = "player_uuid";
	public static final String COLUMN_COSMETICS = "cosmetics";
	public static final String COLUMN_CHESTS = "chests";
	
	public NurCosmeticsPlugin p;
	
	public NurCosmetics(NurCosmeticsPlugin plugin) {
		this.p = plugin;
	}
	
	public void onEnable() {
		
		if(!MySQLConnectionUtil.loadVars()) {
			p.getLogger().log(Level.WARNING, "plugin/Database/config.yml doesn't exist!");
			p.getLogger().log(Level.WARNING, "Disabling Plugin!");
			p.onDisable();
			return;
		}
		
		Database.getRepository(TABLE_COSMETIC, TABLE_COSMETIC);
		
		
		PetStorage.r();
		CosmeticStorage.r();
		NoteBlockAPI.onEnable();
		
		p.getCommand("cosmetics").setExecutor(new CosmeticsCmd());

		Bukkit.getPluginManager().registerEvents(new PlayerListener(), p);
		Bukkit.getPluginManager().registerEvents(new PetListener(), p);
		Bukkit.getPluginManager().registerEvents(new EffectListener(), p);
		Bukkit.getPluginManager().registerEvents(new GardrobeListener(), p);
		Bukkit.getPluginManager().registerEvents(new GadgetListener(), p);
		
		new PlayerItemListener(p);
		
		Connection con = null;
		try {
			con = MySQLConnectionUtil.getNewConnection();
			CreateTables.createAllTables(con);
			
			for(Player p : Bukkit.getOnlinePlayers()) {
				if(CosmeticStorage.getCosmeticPlayer(p.getUniqueId()) == null) {
					CosmeticStorage.loadPlayer(p.getUniqueId(), con);
				}
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		MySQLConnectionUtil.closeConnection(con);
		
	}
	
	public void onDisable() {
		
		for(Entity e : PetStorage.getEntitys()) {
			e.remove();
		}
		
		Bukkit.getScheduler().cancelTasks(this.p);
		
		NoteBlockAPI.onDisable();
	}
	
	public DyeColor fromClay(byte data) {
		if(data == 0) return DyeColor.WHITE;
		if(data == 1) return DyeColor.ORANGE;
		if(data == 2) return DyeColor.MAGENTA;
		if(data == 3) return DyeColor.LIGHT_BLUE;
		if(data == 4) return DyeColor.YELLOW;
		if(data == 5) return DyeColor.LIME;
		if(data == 6) return DyeColor.PINK;
		if(data == 7) return DyeColor.GRAY;
		if(data == 8) return DyeColor.SILVER;
		if(data == 9) return DyeColor.CYAN;
		if(data == 10) return DyeColor.PURPLE;
		if(data == 11) return DyeColor.BLUE;
		if(data == 12) return DyeColor.BROWN;
		if(data == 13) return DyeColor.GREEN;
		if(data == 14) return DyeColor.RED;
		if(data == 15) return DyeColor.BLACK;
		
		return DyeColor.WHITE;
	}

}
