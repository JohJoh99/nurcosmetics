package de.johjoh.nurcosmetics.listener;

import java.sql.Connection;
import java.sql.SQLException;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import de.johjoh.nurcosmetics.NurCosmetics;
import de.johjoh.nurcosmetics.cosmetics.Cosmetic;
import de.johjoh.nurcosmetics.cosmetics.music.NoteBlockAPI;
import de.johjoh.nurcosmetics.sql.MySQLConnectionUtil;
import de.johjoh.nurcosmetics.util.CosmeticPlayer;
import de.johjoh.nurcosmetics.util.CosmeticStorage;
import de.johjoh.nurcosmetics.util.item.NamedItem;
import net.nurserver.core.profile.ProfileLoader;

public class PlayerListener implements Listener {
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e) {
		
		Connection con = null;
		try {
			con = MySQLConnectionUtil.getNewConnection();
			CosmeticPlayer.insert(e.getPlayer().getUniqueId(), con);
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		finally {
			MySQLConnectionUtil.closeConnection(con);
		}
		
		NamedItem item = new NamedItem(Material.DOUBLE_PLANT, NurCosmetics.ITEM_NAME);
		
		e.getPlayer().getInventory().setItem(8, item);
		
	}
	
	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent e) {
		if(!CosmeticStorage.getCosmeticPlayers().containsKey(e.getPlayer().getUniqueId())) return;
		CosmeticStorage.getCosmeticPlayer(e.getPlayer().getUniqueId()).unregisterItems();
		if(CosmeticStorage.getCosmeticPlayer(e.getPlayer().getUniqueId()).getPet() != null)
			CosmeticStorage.getCosmeticPlayer(e.getPlayer().getUniqueId()).getPet().despawn();
		CosmeticStorage.removeSummoner(e.getPlayer().getUniqueId());
		NoteBlockAPI.stopPlaying(e.getPlayer());
		
		ProfileLoader.saveProfile(e.getPlayer());
	}
	
	@EventHandler
	public void onInventoryClick(InventoryClickEvent e) {
		if(e.getCurrentItem() != null && e.getCurrentItem().hasItemMeta() && e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(NurCosmetics.ITEM_NAME)) {
			
		}
		if(e.getInventory().getTitle().contains("H\u00fcte")) {
			if(e.getCurrentItem() != null && e.getCurrentItem().getType() == Material.SKULL_ITEM && e.getCurrentItem().hasItemMeta()) {
				e.setCancelled(true);
				CosmeticPlayer player = CosmeticStorage.getCosmeticPlayer(e.getWhoClicked().getUniqueId());
				Cosmetic cm = Cosmetic.fromDisplayName(e.getCurrentItem().getItemMeta().getDisplayName());
				if(cm == null) {
					return;
				}
				((Player) e.getWhoClicked()).playSound(e.getWhoClicked().getEyeLocation(), Sound.CLICK, 1.0F, 1.0F);
				if(player.hasCosmetic(cm.getId())) {
					
					if(player.enable(cm)) {
						player.getPlayer().sendMessage(NurCosmetics.PREFIX + ChatColor.YELLOW + "Du hast " + cm.getDisplayName() + ChatColor.GRAY + " (" + cm.getCategory().getName() + ")" + ChatColor.YELLOW + " aktiviert");
					}
					else {
						player.getPlayer().sendMessage(NurCosmetics.PREFIX + cm.getDisplayName() + ChatColor.GRAY + " (" + cm.getCategory().getName() + ")" + ChatColor.RED + " ist bereits aktiviert");
					}
					
				}
				else {
					if(player.hasMoreCoins(cm.getPrice())) {
						player.getInventorys().getBuyInventory().setItem(4, cm.getItemStack(false, false, true, false));
						player.getPlayer().openInventory(player.getInventorys().getBuyInventory());
						
					}
					else {
						player.getPlayer().sendMessage(ChatColor.DARK_RED + "Nicht gen\u00fcgend Coins");
						return;
					}
				}
				
			}
		}
		if(e.getInventory().getName().equalsIgnoreCase(ChatColor.GOLD + "Kiste \u00f6ffnen") || e.getInventory().getName().contains("kaufen") || e.getInventory().getName().contains("Einstellungen") || e.getInventory().getName().contains(ChatColor.GOLD + "Cosmetics"))
			e.setCancelled(true);
	}
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e) {
		if(e.getItem() == null || !e.getItem().hasItemMeta()) return;
		if(e.getItem().getItemMeta().getDisplayName().equalsIgnoreCase(NurCosmetics.ITEM_NAME)) {
			e.setCancelled(true);
			if(e.getAction().equals(Action.LEFT_CLICK_AIR) || e.getAction().equals(Action.LEFT_CLICK_BLOCK)) return;
			if(!CosmeticStorage.isRegistered(e.getPlayer().getUniqueId())) {
				Connection con = null;
				try {
					con = MySQLConnectionUtil.getNewConnection();
					CosmeticStorage.loadPlayer(e.getPlayer().getUniqueId(), con);
				} catch (SQLException ex) {
					ex.printStackTrace();
				}
				MySQLConnectionUtil.closeConnection(con);
			}
			
			CosmeticPlayer cp = CosmeticStorage.getCosmeticPlayer(e.getPlayer().getUniqueId());
			
			
			e.getPlayer().openInventory(cp.getInventorys().getMainInventory());
		}
	}
	
	
	
}
