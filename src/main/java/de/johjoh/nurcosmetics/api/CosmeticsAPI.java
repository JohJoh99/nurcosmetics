package de.johjoh.nurcosmetics.api;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.UUID;

import de.johjoh.nurcosmetics.util.CosmeticPlayer;
import de.johjoh.nurcosmetics.util.CosmeticStorage;

public class CosmeticsAPI {
	
	/**@param uniqueId Ziel-Spieler
	 * @param amount Anzahl
	 * @param con MySQL-Connection
	 * @throws SQLException
	 * @return void */
	public static void addChest(UUID uniqueId, int amount, Connection con) throws SQLException {
		CosmeticPlayer cp = CosmeticStorage.getCosmeticPlayer(uniqueId);
		if(cp != null)
			cp.addChest(amount, con);
	}

}
