package de.johjoh.nurcosmetics.util.item;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

public class ItemCreator {
	
	public static ItemStack namedItem(Material m, String name) {
		return namedItem(m, name, new ArrayList<String>(), 1, 0, (byte) 0);
	}
	
	public static ItemStack namedItem(Material m, String name, int amount, int durability) {
		return namedItem(m, name, new ArrayList<String>(), amount, durability, (byte) 0);
	}
	
	public static ItemStack namedItem(Material m, String name, String[] lore) {
		return namedItem(m, name, Arrays.asList(lore), 1, (short) 0, (byte) 0);
	}
	
	public static ItemStack namedItem(Material m, String name, List<String> lore) {
		return namedItem(m, name, lore, 1, 0, (byte) 0);
	}
	
	public static ItemStack namedItem(Material m, String name, String[] lore, byte data) {
		return namedItem(m, name, Arrays.asList(lore), 1, 0, data);
	}
	
	public static ItemStack namedItem(Material m, String name, short data) {
		ItemStack is = new ItemStack(m, 1, data);
		ItemMeta meta = is.getItemMeta();
		meta.setDisplayName(name);
		is.setItemMeta(meta);
		return is;
	}
	
	public static ItemStack namedItem(Material m, String name, List<String> lore, int amount, int durability, byte data) {
		ItemStack is = new ItemStack(m, amount, data);
		ItemMeta meta = is.getItemMeta();
		meta.setDisplayName(name);
		if (lore != null && !lore.isEmpty()) {
			meta.setLore(lore);
		} else {
			meta.setLore(null);
		}
		is.setItemMeta(meta);
		is.setDurability((short) durability);
		return is;
	}
	
	public static ItemStack namedSkull(String name, String owner) {
		ItemStack is = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
		SkullMeta meta = (SkullMeta) is.getItemMeta();
		meta.setDisplayName(name);
		meta.setOwner(owner);
		is.setItemMeta(meta);
		return is;
	}
	
}
