package de.johjoh.nurcosmetics.util.item;

import de.johjoh.nurcosmetics.util.CosmeticPlayer;

public interface PlayerRunnable {
	
	public void run(CosmeticPlayer player);

}
