package de.johjoh.nurcosmetics.util.item;

import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.Material;
import org.bukkit.event.block.Action;
import org.bukkit.inventory.meta.ItemMeta;

import de.johjoh.nurcosmetics.util.CosmeticPlayer;

public class InteractableItem extends NamedItem {

	private PlayerRunnable runnable = null;
	private boolean runOnInteract = true;
	private boolean runOnClick = true;
	private ArrayList<Action> interactActions = new ArrayList<Action>();
	private UUID player;
	
	public InteractableItem(UUID player, Material m, String name, PlayerRunnable runnable, String... lore) {
		super(m, name, lore);
		this.player = player;
		this.runnable = runnable;
		interactActions.add(Action.RIGHT_CLICK_AIR);
		interactActions.add(Action.RIGHT_CLICK_BLOCK);
		register();
	}
	
	public InteractableItem(UUID player, Material m, String name, PlayerRunnable runnable, int amount, String... lore) {
		super(m, name, amount, lore);
		this.player = player;
		this.runnable = runnable;
		interactActions.add(Action.RIGHT_CLICK_AIR);
		interactActions.add(Action.RIGHT_CLICK_BLOCK);
		register();
	}
	
	public InteractableItem(UUID player, Material m, String name, int amount, short data, PlayerRunnable runnable, String... lore) {
		super(m, name, amount, data, lore);
		this.player = player;
		this.runnable = runnable;
		interactActions.add(Action.RIGHT_CLICK_AIR);
		interactActions.add(Action.RIGHT_CLICK_BLOCK);
		register();
	}
	
	public InteractableItem(UUID player, Material m, String name, ItemMeta meta, PlayerRunnable runnable, String... lore) {
		super(m, name, meta, lore);
		this.player = player;
		this.runnable = runnable;
		interactActions.add(Action.RIGHT_CLICK_AIR);
		interactActions.add(Action.RIGHT_CLICK_BLOCK);
		register();
	}
	
	public void register() {
		PlayerItemListener.getInstance().addItem(this);
	}
	
	public void unregister() {
		PlayerItemListener.getInstance().removeItem(this.getMetaObject());
	}
	
	@Override
	public boolean setItemMeta(ItemMeta itemMeta) {
		unregister();
		boolean b = super.setItemMeta(itemMeta);
		register();
		return b;
	}
	
	public MetaObject getMetaObject() {
		return new MetaObject(this.getPlayerUUID(), this.getType(), this.getItemMeta(), this.getAmount(), this.getDurability());
	}
	
	public void runOnInteract(boolean flag, Action... interactActions) {
		this.runOnInteract = flag;
		this.interactActions.clear();
		if(this.runOnInteract && interactActions != null) {
			for(Action a : interactActions) this.interactActions.add(a);
		}
	}
	
	public void runOnClick(boolean flag) {
		this.runOnClick = flag;
	}
	
	public boolean getUnregisterAfterInvClose() {
		return true;
	}
	
	public boolean getRunOnInteract() {
		return this.runOnInteract;
	}
	
	public ArrayList<Action> getRunOnInteractActions() {
		return this.interactActions;
	}
	
	public boolean getRunOnClick() {
		return this.runOnClick;
	}
	
	public UUID getPlayerUUID() {
		return player;
	}
	
	protected void run(CosmeticPlayer cosmeticPlayer) {
		runnable.run(cosmeticPlayer);
	}
	
	public static class MetaObject {
		
		private final UUID player;
		private final Material itemMaterial;
		private final ItemMeta meta;
		private final int amount;
		private final short damage;
		
		public MetaObject(UUID player, Material itemMaterial, ItemMeta meta, int amount, short damage) {
			this.player = player;
			this.itemMaterial = itemMaterial;
			this.meta = meta;
			this.amount = amount;
			this.damage = damage;
		}
		
		public UUID getPlayer() {
			return this.player;
		}
		
		public Material getMaterial() {
			return itemMaterial;
		}
		
		public ItemMeta getMeta() {
			return meta;
		}
		
		public int getAmount() {
			return amount;
		}
		
		public short getDamage() {
			return damage;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + amount;
			result = prime * result + damage;
			result = prime * result + ((itemMaterial == null) ? 0 : itemMaterial.hashCode());
			result = prime * result + ((meta == null) ? 0 : meta.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if(this == obj)
				return true;
			if(obj == null)
				return false;
			if(getClass() != obj.getClass())
				return false;
			MetaObject other = (MetaObject) obj;
			if(player != other.player)
				return false;
			if(amount != other.amount)
				return false;
			if(damage != other.damage)
				return false;
			if(itemMaterial != other.itemMaterial)
				return false;
			if(meta == null) {
				if(other.meta != null)
					return false;
			} else if(!meta.equals(other.meta))
				return false;
			return true;
		}

	}

}
