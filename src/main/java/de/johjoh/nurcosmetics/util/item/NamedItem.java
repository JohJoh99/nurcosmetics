package de.johjoh.nurcosmetics.util.item;

import java.util.ArrayList;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class NamedItem extends ItemStack {
	
	private String name;
	private String[] lore;
	private ItemMeta meta;
	
	public NamedItem(Material m, String name, String... lore) {
		super(m);
		this.name = name;
		this.lore = lore;
		constructor();
	}
	
	public NamedItem(Material m, String name, int amount, String... lore) {
		super(m, amount);
		this.name = name;
		this.lore = lore;
		constructor();
	}
	
	public NamedItem(Material m, String name, int amount, short data, String... lore) {
		super(m, amount, data);
		this.name = name;
		this.lore = lore;
		constructor();
	}
	
	public NamedItem(Material m, String name, ItemMeta meta, String... lore) {
		super(m);
		this.name = name;
		this.lore = lore;
		this.meta = meta;
		constructor();
	}
	
	private void constructor() {
		ItemMeta meta = this.meta != null ? this.meta : super.getItemMeta();
		if(lore != null && lore.length > 0) {
			ArrayList<String> lore = new ArrayList<String>();
			for(String s : lore) {
				lore.add(s);
			}
			meta.setLore(lore);	
		}
		
		if(name != null) {
			meta.setDisplayName(name);
		}
		
		super.setItemMeta(meta);
	}

}
