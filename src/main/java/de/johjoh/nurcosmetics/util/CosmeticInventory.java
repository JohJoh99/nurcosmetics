package de.johjoh.nurcosmetics.util;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Random;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;

import de.johjoh.nurcosmetics.NurCosmetics;
import de.johjoh.nurcosmetics.cosmetics.Category;
import de.johjoh.nurcosmetics.cosmetics.Cosmetic;
import de.johjoh.nurcosmetics.cosmetics.effect.EffectListener;
import de.johjoh.nurcosmetics.cosmetics.music.NoteBlockAPI;
import de.johjoh.nurcosmetics.sql.MySQLConnectionUtil;
import de.johjoh.nurcosmetics.util.item.InteractableItem;
import de.johjoh.nurcosmetics.util.item.ItemCreator;
import de.johjoh.nurcosmetics.util.item.PlayerRunnable;

public class CosmeticInventory {
	
	private Inventory mainInventory;
	private final UUID ownerUniqueId;
	private InteractableItem chestsItem;
	private HashMap<Category, Inventory> subInventorys = new HashMap<Category, Inventory>();
	private Inventory buyInventory;
	private Inventory chestInventory;
	private InteractableItem grnPane, redPane;
	
	public CosmeticInventory(int chests, CosmeticPlayer p) {
		ownerUniqueId = p.getUniqueId();
		
		mainInventory = Bukkit.createInventory(null, 54, NurCosmetics.ITEM_NAME);
		mainInventory.setMaxStackSize(512);
		
		int[] i = new int[]{10, 12, 14, 16, 28, 30, 32, 34};
		int in = 0;
		for(final Category c : Category.values()) {
			
			InteractableItem ci = new InteractableItem(p.getUniqueId(), c.getMaterial(), c.getDisplayName(), 1, (short) c.getData(), new PlayerRunnable() {
				public void run(CosmeticPlayer player) {
					player.getPlayer().playSound(player.getPlayer().getEyeLocation(), Sound.CLICK, 1.0F, 1.0F);
					
					player.getPlayer().openInventory(subInventorys.get(c));
					
				}
			});
			ci.runOnClick(true);
			
			mainInventory.setItem(i[in], ci);
			in++;
		}
		
		chestsItem = new InteractableItem(p.getUniqueId(), Material.ENDER_CHEST, ChatColor.GOLD + "Kisten", chests, (short) 0, new PlayerRunnable() {
			public void run(CosmeticPlayer player) {
				player.getPlayer().playSound(player.getPlayer().getEyeLocation(), Sound.CLICK, 1.0F, 1.0F);
				CosmeticPlayer cp = CosmeticStorage.getCosmeticPlayer(ownerUniqueId);
				
				if(cp.getChests() <= 0) {
					cp.getPlayer().sendMessage(NurCosmetics.PREFIX + ChatColor.RED + "Du besitzt keine Kisten");
					return;
				}
				
				cp.getPlayer().openInventory(chestInventory);
				
			}
		}, ChatColor.GRAY + "Klicke zum Öffnen");
		chestsItem.runOnClick(true);
		mainInventory.setItem(52, chestsItem);
		
		InteractableItem close = new InteractableItem(p.getUniqueId(), Material.BARRIER, ChatColor.RED + "Schließen", new PlayerRunnable() {
			public void run(CosmeticPlayer player) {
				player.getPlayer().playSound(player.getPlayer().getEyeLocation(), Sound.CLICK, 1.0F, 1.0F);
				player.getPlayer().closeInventory();
			}
		});
		close.runOnClick(true);
		mainInventory.setItem(53, close);
		
		
		for(Category c : Category.values()) {
			updateSubInventory(p, c);
		}
		
		
		

		buyInventory = Bukkit.createInventory(null, 9, ChatColor.GOLD + "Item kaufen");
		
		ItemStack gs = new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte) 15);
		ItemMeta gm = gs.getItemMeta();
		gm.setDisplayName(" ");
		gs.setItemMeta(gm);
		
		for(int it = 0; it < 9; it++) buyInventory.setItem(it, gs);
		
		InteractableItem buy = new InteractableItem(p.getUniqueId(), Material.STAINED_CLAY, ChatColor.GREEN + "Kaufen", 1, (short) 5, new PlayerRunnable() {
			public void run(CosmeticPlayer player) {
				player.getPlayer().playSound(player.getPlayer().getEyeLocation(), Sound.CLICK, 1.0F, 1.0F);
				
				Cosmetic c = Cosmetic.fromDisplayName(buyInventory.getItem(4).getItemMeta().getDisplayName());
				if(c == null) {
					player.getPlayer().openInventory(mainInventory);
					return;
				}
				
				Connection con = null;
				
				try {
					con = MySQLConnectionUtil.getNewConnection();
					player.buyCosmetic(c, true, con);
				} catch (SQLException ex) {
					ex.printStackTrace();
				}
				
				MySQLConnectionUtil.closeConnection(con);
				
				for(ItemStack is : subInventorys.get(c.getCategory())) {
					if(is == null || !is.hasItemMeta()) continue;
//					is.setItemMeta(c.getItemStack(true, true, false).getItemMeta());
//					if(is.getItemMeta().getDisplayName().equalsIgnoreCase(c.getDisplayName()) && is.getType() != Material.SKULL_ITEM) {
//						InteractableItem it = PlayerItemListener.getInstance().getItem(new MetaObject(is.getType(), is.getItemMeta(), is.getAmount(), is.getDurability()));
//						it.unregister();
//						it.setItemMeta(c.getItemStack(true, true, false).getItemMeta());
//						it.register();
//					}
					updateSubInventory(player, c.getCategory());
				}

				player.getPlayer().openInventory(subInventorys.get(c.getCategory()));
			}
		});
		buy.runOnClick(true);
		buyInventory.setItem(1, buy);
		
		
		InteractableItem cancel = new InteractableItem(p.getUniqueId(), Material.STAINED_CLAY, ChatColor.RED + "Abbrechen", 1, (short) 14, new PlayerRunnable() {
			public void run(CosmeticPlayer player) {
				player.getPlayer().playSound(player.getPlayer().getEyeLocation(), Sound.CLICK, 1.0F, 1.0F);
				Cosmetic c = Cosmetic.fromDisplayName(buyInventory.getItem(4).getItemMeta().getDisplayName());
				if(c == null) {
					player.getPlayer().openInventory(mainInventory);
					return;
				}
				player.getPlayer().openInventory(subInventorys.get(c.getCategory()));
			}
		});
		buyInventory.setItem(7, cancel);
		
		
		

		
		chestInventory = Bukkit.createInventory(null, 45, ChatColor.GOLD + "Kiste �ffnen");
		
		grnPane = new InteractableItem(p.getUniqueId(), Material.STAINED_GLASS_PANE, ChatColor.GREEN + "\u00f6ffnen",  1,  (short) 5, new PlayerRunnable() {
			public void run(CosmeticPlayer player) {
				player.getPlayer().playSound(player.getPlayer().getEyeLocation(), Sound.CLICK, 1.0F, 1.0F);
				
				if(player.getChests() <= 0) {
					player.getPlayer().openInventory(mainInventory);
					player.getPlayer().sendMessage(NurCosmetics.PREFIX + ChatColor.RED + "Du besitzt keine Kisten");
					return;
				}
				
				ArrayList<Cosmetic> pool = new ArrayList<Cosmetic>();
				for(Cosmetic c : Cosmetic.values()) {
					if(!player.hasCosmetic(c.getId())) {
						for(int i = 0; i < 9; i++) {
							pool.add(c);
						}
					}
				}
				Collections.shuffle(pool);
				
				if(pool.size() <= 0) {
					player.getPlayer().sendMessage(NurCosmetics.PREFIX + ChatColor.RED + "Du besitzt bereits alle Gegenst\u00e4nde");
					return;
				}
				
				Connection con = null;
				
				try {
					con = MySQLConnectionUtil.getNewConnection();
					player.addChest(-1, con);
				} catch (SQLException ex) {
					ex.printStackTrace();
				}
				
				MySQLConnectionUtil.closeConnection(con);
				
				int ticks = new Random().nextInt(100)+50;
				
				new ChestThread(ticks, pool, player.getUniqueId()).start();
				
				
			}
		});
		
		redPane = new InteractableItem(p.getUniqueId(), Material.STAINED_GLASS_PANE, ChatColor.RED + "Abbrechen",  1,  (short) 14, new PlayerRunnable() {
			public void run(CosmeticPlayer player) {
				player.getPlayer().playSound(player.getPlayer().getEyeLocation(), Sound.CLICK, 1.0F, 1.0F);
				player.getPlayer().openInventory(mainInventory);
			}
		});
		
		for(int it = 0; it < 45; it++) {
			if(it < 18) chestInventory.setItem(it, grnPane);
			if(it > 17 && it < 27) chestInventory.setItem(it, ItemCreator.namedSkull(" ", "MHF_Question"));
			if(it > 26) chestInventory.setItem(it, redPane);
		}
		
		chestInventory.setItem(22, ItemCreator.namedItem(Material.ENDER_CHEST, ChatColor.GOLD + "Kiste"));
		
	}
	
	public void resetChestInventory() {
		chestInventory.clear();
		for(int it = 0; it < 45; it++) {
			if(it < 18) chestInventory.setItem(it, grnPane);
			if(it > 17 && it < 27) chestInventory.setItem(it, ItemCreator.namedSkull(" ", "MHF_Question"));
			if(it > 26) chestInventory.setItem(it, redPane);
		}
		chestInventory.setItem(22, ItemCreator.namedItem(Material.ENDER_CHEST, ChatColor.GOLD + "Kiste"));
	}
	
	@SuppressWarnings("unused")
	public void updateSubInventory(CosmeticPlayer p, Category c) {

//		if(c != Category.CLOTHES) {
		if(true) {
			
			Inventory subInventory = Bukkit.createInventory(null, 36, NurCosmetics.ITEM_NAME + ChatColor.DARK_GRAY + " - " + ChatColor.GREEN + c.getDisplayName());

			subInventory.setItem(17, ItemCreator.namedItem(Material.STAINED_GLASS_PANE, " ", 1, 15));
			subInventory.setItem(26, ItemCreator.namedItem(Material.STAINED_GLASS_PANE, " ", 1, 15));

			if(c == Category.MUSIC) {
				InteractableItem stop = new InteractableItem(p.getUniqueId(), Material.REDSTONE, ChatColor.RED + "Stop", new PlayerRunnable() {
					public void run(CosmeticPlayer player) {
						player.getPlayer().playSound(player.getPlayer().getEyeLocation(), Sound.CLICK, 1.0F, 1.0F);
						NoteBlockAPI.stopPlaying(player.getPlayer());
						player.getPlayer().sendMessage(NurCosmetics.PREFIX + ChatColor.RED + "Stoppe Musik...");
					}
				});
				stop.runOnClick(true);
				subInventory.setItem(8, stop);
			}

			if(c == Category.HATS) {
				InteractableItem remove = new InteractableItem(p.getUniqueId(), Material.REDSTONE, ChatColor.RED + "Hut entfernen", new PlayerRunnable() {
					public void run(CosmeticPlayer player) {
						player.getPlayer().playSound(player.getPlayer().getEyeLocation(), Sound.CLICK, 1.0F, 1.0F);
						player.getPlayer().getInventory().setHelmet(null);
						player.getPlayer().sendMessage(NurCosmetics.PREFIX + ChatColor.GRAY + "Dein Hut wurde entfernt");
					}
				});
				remove.runOnClick(true);
				subInventory.setItem(8, remove);
			}

			if(c == Category.EFFECTS) {
				InteractableItem remove = new InteractableItem(p.getUniqueId(), Material.MILK_BUCKET, ChatColor.RED + "Effekte entfernen", new PlayerRunnable() {
					public void run(CosmeticPlayer player) {
						player.getPlayer().playSound(player.getPlayer().getEyeLocation(), Sound.CLICK, 1.0F, 1.0F);
						for(PotionEffect pe : player.getPlayer().getActivePotionEffects())
							player.getPlayer().removePotionEffect(pe.getType());
						player.getPlayer().sendMessage(NurCosmetics.PREFIX + ChatColor.GRAY + "Dein Effekt wurde entfernt");
					}
				});
				subInventory.setItem(8, remove);
			}

			if(c == Category.PARTICLE_TRAIL) {
				InteractableItem remove = new InteractableItem(p.getUniqueId(), Material.REDSTONE, ChatColor.RED + "Partikel entfernen", new PlayerRunnable() {
					public void run(CosmeticPlayer player) {
						player.getPlayer().playSound(player.getPlayer().getEyeLocation(), Sound.CLICK, 1.0F, 1.0F);
						EffectListener.unregisterParticle(player.getUniqueId());
						player.getPlayer().sendMessage(NurCosmetics.PREFIX + ChatColor.GRAY + "Deine Partikelspur wurde entfernt");
					}
				});
				subInventory.setItem(8, remove);
			}
			
			if(c == Category.PETS) {
				InteractableItem remove = new InteractableItem(p.getUniqueId(), Material.REDSTONE, ChatColor.RED + "Haustier entfernen", new PlayerRunnable() {
					public void run(CosmeticPlayer player) {
						player.getPlayer().playSound(player.getPlayer().getEyeLocation(), Sound.CLICK, 1.0F, 1.0F);
						player.getPet().despawn();
						player.setPet(null);
						player.getPlayer().sendMessage(NurCosmetics.PREFIX + ChatColor.GRAY + "Deine Haustier wurde entfernt");
					}
				});
				remove.runOnClick(true);
				subInventory.setItem(8, remove);
			}
			
			if(c == Category.GADGETS) {
				InteractableItem remove = new InteractableItem(p.getUniqueId(), Material.REDSTONE, ChatColor.RED + "Gadget entfernen", new PlayerRunnable() {
					public void run(CosmeticPlayer player) {
						player.getPlayer().getInventory().setItem(6, null);
					}
				});
				remove.runOnClick(true);
				subInventory.setItem(8, remove);
			}
			
			InteractableItem back = new InteractableItem(p.getUniqueId(), Material.BARRIER, ChatColor.RED + "Zur\u00fcck", new PlayerRunnable() {
				public void run(CosmeticPlayer player) {
					player.getPlayer().playSound(player.getPlayer().getEyeLocation(), Sound.CLICK, 1.0F, 1.0F);
					player.getPlayer().openInventory(mainInventory);
				}
			});
			subInventory.setItem(35, back);
			
			int i = 0;
			for(final Cosmetic cm : Cosmetic.values()) {
				if(cm.getCategory() == c) {
					
					ItemStack is = cm.getItemStack(false, p.hasCosmetic(cm.getId()), p.hasMoreCoins(cm.getPrice()), true);
					InteractableItem item = new InteractableItem(p.getUniqueId(), is.getType(), is.getItemMeta().getDisplayName(), 1, (short) cm.getData(), new PlayerRunnable() {
						public void run(CosmeticPlayer player) {
							player.getPlayer().playSound(player.getPlayer().getEyeLocation(), Sound.CLICK, 1.0F, 1.0F);
							
							if(player.hasCosmetic(cm.getId())) {
								
								if(player.enable(cm)) {
									player.getPlayer().sendMessage(NurCosmetics.PREFIX + ChatColor.YELLOW + "Du hast " + cm.getDisplayName() + ChatColor.GRAY + " (" + cm.getCategory().getName() + ")" + ChatColor.YELLOW + " aktiviert");
								} else {
									player.getPlayer().sendMessage(NurCosmetics.PREFIX + cm.getDisplayName() + ChatColor.GRAY + " (" + cm.getCategory().getName() + ")" + ChatColor.RED + " ist bereits aktiviert");
								}
								
							} else {
								if(player.hasMoreCoins(cm.getPrice())) {
									buyInventory.setItem(4, cm.getItemStack(false, false, true, false));
									player.getPlayer().openInventory(buyInventory);
									
								} else {
									player.getPlayer().sendMessage(NurCosmetics.PREFIX + ChatColor.RED + "Du besitzt nicht gen\u00fcgend Coins");
									return;
								}
							}
						}
					});
					item.unregister();
					ItemMeta meta = is.getItemMeta();
					meta.setLore(is.getItemMeta().getLore());
					item.setItemMeta(meta);
					item.register();
					subInventory.setItem(i, item);
					
					if(i == 6 || i == 15 || i == 24 || i == 33) i+=3;
						else i++;
				}
			}
			
			subInventorys.put(c, subInventory);
			
		}
		else {
			
			Inventory subInventory = Bukkit.createInventory(null, 54, NurCosmetics.ITEM_NAME + ChatColor.DARK_GRAY + " - " + ChatColor.GREEN + c.getDisplayName());
			
			Material hm = Material.BARRIER;
			if(p.getPlayer().getInventory().getHelmet() != null) hm = p.getPlayer().getInventory().getHelmet().getType();
			InteractableItem head = new InteractableItem(p.getUniqueId(), hm, ChatColor.GOLD + "Helm", new PlayerRunnable() {
				public void run(CosmeticPlayer player) {
					player.getPlayer().playSound(player.getPlayer().getEyeLocation(), Sound.CLICK, 1.0F, 1.0F);
					
				}
			});
			subInventory.setItem(13, head);
			
			Material cm = Material.BARRIER;
			if(p.getPlayer().getInventory().getChestplate() != null) cm = p.getPlayer().getInventory().getChestplate().getType();
			InteractableItem chest = new InteractableItem(p.getUniqueId(), cm, ChatColor.GOLD + "Brustplatte", new PlayerRunnable() {
				public void run(CosmeticPlayer player) {
					player.getPlayer().playSound(player.getPlayer().getEyeLocation(), Sound.CLICK, 1.0F, 1.0F);
					
				}
			});
			subInventory.setItem(22, chest);
			
			Material lm = Material.BARRIER;
			if(p.getPlayer().getInventory().getLeggings() != null) lm = p.getPlayer().getInventory().getLeggings().getType();
			InteractableItem leggings = new InteractableItem(p.getUniqueId(), lm, ChatColor.GOLD + "Hose", new PlayerRunnable() {
				public void run(CosmeticPlayer player) {
					player.getPlayer().playSound(player.getPlayer().getEyeLocation(), Sound.CLICK, 1.0F, 1.0F);
					
				}
			});
			subInventory.setItem(31, leggings);
			
			Material bm = Material.BARRIER;
			if(p.getPlayer().getInventory().getHelmet() != null) bm = p.getPlayer().getInventory().getHelmet().getType();
			InteractableItem boots = new InteractableItem(p.getUniqueId(), bm, ChatColor.GOLD + "Schuhe", new PlayerRunnable() {
				public void run(CosmeticPlayer player) {
					player.getPlayer().playSound(player.getPlayer().getEyeLocation(), Sound.CLICK, 1.0F, 1.0F);
					
				}
			});
			subInventory.setItem(40, boots);
			
			subInventory.setItem(11, ItemCreator.namedSkull(ChatColor.GOLD + "Vorheriger Helm", "MHF_ArrowLeft"));
			subInventory.setItem(20, ItemCreator.namedSkull(ChatColor.GOLD + "Vorheriger Brustplatte", "MHF_ArrowLeft"));
			subInventory.setItem(29, ItemCreator.namedSkull(ChatColor.GOLD + "Vorheriger Hose", "MHF_ArrowLeft"));
			subInventory.setItem(38, ItemCreator.namedSkull(ChatColor.GOLD + "Vorheriger Schuhe", "MHF_ArrowLeft"));
			
			subInventory.setItem(15, ItemCreator.namedSkull(ChatColor.GOLD + "N\u00e4chster Helm", "MHF_ArrowRight"));
			subInventory.setItem(24, ItemCreator.namedSkull(ChatColor.GOLD + "N\u00e4chste Brustplatte", "MHF_ArrowRight"));
			subInventory.setItem(33, ItemCreator.namedSkull(ChatColor.GOLD + "N\u00e4chste Hose", "MHF_ArrowRight"));
			subInventory.setItem(42, ItemCreator.namedSkull(ChatColor.GOLD + "N\u00e4chste Schuhe", "MHF_ArrowRight"));
			
			
			
			
			
			
			
			
			
			InteractableItem back = new InteractableItem(p.getUniqueId(), Material.BARRIER, ChatColor.RED + "Zurück", new PlayerRunnable() {
				public void run(CosmeticPlayer player) {
					player.getPlayer().playSound(player.getPlayer().getEyeLocation(), Sound.CLICK, 1.0F, 1.0F);
					player.getPlayer().openInventory(mainInventory);
				}
			});
			subInventory.setItem(53, back);
			
			subInventorys.put(c, subInventory);
		}
	}
	
	public Inventory getMainInventory() {
		return this.mainInventory;
	}
	
	public Inventory getSubInventory(Category c) {
		return this.subInventorys.get(c);
	}
	
	public Inventory getBuyInventory() {
		return this.buyInventory;
	}
	
	public Inventory getChestInventory() {
		return this.chestInventory;
	}
	
	public InteractableItem getChestsItem() {
		return this.chestsItem;
	}
	
	
}
