package de.johjoh.nurcosmetics.util;

import static de.johjoh.nurcosmetics.NurCosmetics.COLUMN_CHESTS;
import static de.johjoh.nurcosmetics.NurCosmetics.COLUMN_COSMETICS;
import static de.johjoh.nurcosmetics.NurCosmetics.COLUMN_PLAYER_UUID;
import static de.johjoh.nurcosmetics.NurCosmetics.TABLE_COSMETIC;

import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.potion.PotionEffect;

import de.johjoh.nurcosmetics.NurCosmetics;
import de.johjoh.nurcosmetics.NurCosmeticsPlugin;
import de.johjoh.nurcosmetics.cosmetics.Category;
import de.johjoh.nurcosmetics.cosmetics.Cosmetic;
import de.johjoh.nurcosmetics.cosmetics.effect.EffectListener;
import de.johjoh.nurcosmetics.cosmetics.music.NBSDecoder;
import de.johjoh.nurcosmetics.cosmetics.music.NoteBlockAPI;
import de.johjoh.nurcosmetics.cosmetics.music.RadioSongPlayer;
import de.johjoh.nurcosmetics.cosmetics.music.Song;
import de.johjoh.nurcosmetics.cosmetics.music.SongPlayer;
import de.johjoh.nurcosmetics.cosmetics.pet.Pet;
import de.johjoh.nurcosmetics.cosmetics.pet.PetStorage;
import de.johjoh.nurcosmetics.sql.MySQLConnectionUtil;
import de.johjoh.nurcosmetics.sql.MySQLUtil;
import de.johjoh.nurcosmetics.util.item.ItemCreator;
import de.johjoh.nurcosmetics.util.item.NamedItem;
import de.johjoh.nurcosmetics.util.item.PlayerItemListener;
import net.nurserver.core.profile.Profile;
import net.nurserver.core.profile.ProfileManager;

public class CosmeticPlayer {
	
	private UUID uniqueId;
	private ArrayList<Integer> cosmetics;
	private int chests;
	
	private Cosmetic morphType = null;
	private Cosmetic petType = null;
	private Pet pet = null;
	
	private CosmeticInventory inventorys;
	
	public CosmeticPlayer(UUID uniqueId, Connection con) throws SQLException {
		this.uniqueId = uniqueId;
		
		cosmetics = new ArrayList<Integer>();
		
		String cm = null;
		
		ResultSet rs = MySQLUtil.selectAll(TABLE_COSMETIC, COLUMN_PLAYER_UUID, uniqueId.toString(), con);
		if(rs.last()) {
			cm = rs.getString(COLUMN_COSMETICS);
			chests = rs.getInt(COLUMN_CHESTS);
		}
		
		if(cm != null && !cm.equalsIgnoreCase("NULL")) {
			for(String s : cm.split(",")) {
				try {
					cosmetics.add(Integer.valueOf(s));
				} catch(NumberFormatException e) {
					continue;
				}
			}
		}
		
		inventorys = new CosmeticInventory(chests, this);
		
	}
	
	public UUID getUniqueId() {
		return this.uniqueId;
	}
	
	public ArrayList<Integer> getCosmetics() {
		return this.cosmetics;
	}
	
	public int getChests() {
		return this.chests;
	}
	
	public Cosmetic getMorphCosmetic() {
		return this.morphType;
	}
	
	public Cosmetic getPetCosmetic() {
		return this.petType;
	}
	
	public Pet getPet() {
		return this.pet;
	}
	
	public void setPet(Pet pet) {
		this.pet = pet;
	}
	
	public CosmeticInventory getInventorys() {
		return this.inventorys;
	}
	
	public boolean hasCosmetic(int id) {
		if(cosmetics.contains(id)) return true;
		return false;
	}
	
	public boolean buyCosmetic(Cosmetic cosmetic, boolean removeCoins, Connection con) throws SQLException {
		if(removeCoins) {
			if(!hasMoreCoins(cosmetic.getPrice())) return false;
			Profile p = ProfileManager.getProfile(getPlayer());
			p.setCoins(p.getCoins()-cosmetic.getPrice());
		}
		
		cosmetics.add(cosmetic.getId());
		
		String cs = "";
		for(int i : cosmetics) {
			cs += i + ",";
		}
		
		MySQLUtil.updateString(TABLE_COSMETIC, COLUMN_COSMETICS, cs, COLUMN_PLAYER_UUID, uniqueId.toString(), con);
		if(removeCoins) {
			Bukkit.getPlayer(uniqueId).sendMessage(NurCosmetics.PREFIX + ChatColor.GREEN + "Du hast " + cosmetic.getDisplayName() + ChatColor.GRAY + " (" + cosmetic.getCategory().getName() + ")" + ChatColor.GREEN + " gekauft");
		}
		else {
			getPlayer().sendMessage(NurCosmetics.PREFIX + ChatColor.GREEN + "Du hast " + cosmetic.getDisplayName() + ChatColor.GRAY + " (" + cosmetic.getCategory().getName() + ")" + ChatColor.GREEN + " gezogen");
		}
		
		return true;
	}
	
	public void addChest(int amount, Connection con) throws SQLException {
		chests += amount;
		MySQLUtil.updateInt(TABLE_COSMETIC, COLUMN_CHESTS, chests, COLUMN_PLAYER_UUID, uniqueId.toString(), con);
		inventorys.getChestsItem().unregister();
		inventorys.getChestsItem().setAmount(chests);
		inventorys.getChestsItem().register();
		inventorys.getMainInventory().setItem(52, inventorys.getChestsItem());
	}
	
	public void resetCosmetics() {
		
		cosmetics.clear();
		
		Connection con = null;
		try {
			con = MySQLConnectionUtil.getNewConnection();
			con.prepareStatement("UPDATE `" + TABLE_COSMETIC + "` SET `" + COLUMN_COSMETICS + "` = NULL WHERE `" + COLUMN_PLAYER_UUID + "`='" + uniqueId.toString() + "'").execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		MySQLConnectionUtil.closeConnection(con);
		
		Bukkit.getPlayer(uniqueId).sendMessage(ChatColor.RED + "Cosmetics reseted");
		
	}
	
	public boolean hasMoreCoins(int coins) {
		Profile p = ProfileManager.getProfile(getPlayer());
		if(p.getCoins() >= coins) return true;
		return false;
	}
	
	public boolean enable(Cosmetic c) {
//		if(c.getCategory() == Category.MORPHS) {
//			
//			
//			
//			return true;
//		}
		if(c.getCategory() == Category.PETS) {
			
			petType = c;
			if(pet != null) {
				if(pet.getType() == c) {
					return false;
				}
				pet.despawn();
				pet = null;
			}
			
			pet = new Pet(c, Bukkit.getPlayer(uniqueId));
			pet.spawn(Bukkit.getPlayer(uniqueId).getLocation().add(0.5, 0.5, 0.5));
			
			PetStorage.addPet(uniqueId, pet);
			
			return true;
		}
		else if(c.getCategory() == Category.MUSIC) {
			Player p = Bukkit.getPlayer(uniqueId);
			
			NoteBlockAPI.stopPlaying(p);
			
			Song s = NBSDecoder.parse(new File(NurCosmeticsPlugin.getInstance().getDataFolder(), "/tracks/" + c.getModifier() + ".nbs"));
			SongPlayer sp = new RadioSongPlayer(s);
			sp.setAutoDestroy(true);
			sp.addPlayer(p);
			sp.setPlaying(true);
			
			return true;
		}
		else if(c.getCategory() == Category.HATS) {
			Player p = Bukkit.getPlayer(uniqueId);
			if(c.getModifier() == null) {
				if(p.getInventory().getHelmet() != null && p.getInventory().getHelmet().getType() == c.getItem()) return false;
				p.getInventory().setHelmet(ItemCreator.namedItem(c.getItem(), c.getDisplayName() + " Hat"));
				return true;
			}
			else {
				if(p.getInventory().getHelmet() != null && (p.getInventory().getHelmet().getType() == Material.SKULL_ITEM && ((SkullMeta)p.getInventory().getHelmet().getItemMeta()).getOwner().equalsIgnoreCase((String)c.getModifier()))) return false;
				p.getInventory().setHelmet(ItemCreator.namedSkull(c.getDisplayName() + " Hat", (String)c.getModifier()));
				return true;
			}
		}
		else if(c.getCategory() == Category.EFFECTS) {
			Player p = Bukkit.getPlayer(uniqueId);
			for(PotionEffect pe : p.getActivePotionEffects())
				p.removePotionEffect(pe.getType());
			p.addPotionEffect((PotionEffect) c.getModifier());
			
			return true;
		}
		else if(c.getCategory() == Category.PARTICLE_TRAIL) {
			Player p = Bukkit.getPlayer(uniqueId);
			EffectListener.registerParticle(p.getUniqueId(), (Effect) c.getModifier());
			
			return true;
		}
		else if(c.getCategory() == Category.GADGETS) {
			Player p = Bukkit.getPlayer(uniqueId);
			p.getInventory().setItem(6, new NamedItem(c.getItem(), c.getDisplayName()));
			return true;
		}
		return false;
	}
	
	public Player getPlayer() {
		return Bukkit.getPlayer(uniqueId);
	}
	
	public void unregisterItems() {
		PlayerItemListener.getInstance().unregisterPlayer(uniqueId);
	}
	
	public static void insert(UUID uniqueId, Connection con) throws SQLException {
		ResultSet rs = MySQLUtil.selectAll(TABLE_COSMETIC, COLUMN_PLAYER_UUID, uniqueId.toString(), con);
		if(!rs.last()) {
			MySQLUtil.update("INSERT INTO `" + TABLE_COSMETIC + "` (`" + COLUMN_PLAYER_UUID + "`) VALUES ('" + uniqueId.toString() + "')", con);
		}
	}

}
