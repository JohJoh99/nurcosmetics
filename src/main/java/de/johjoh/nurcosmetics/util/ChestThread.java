package de.johjoh.nurcosmetics.util;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;

import de.johjoh.nurcosmetics.cosmetics.Cosmetic;
import de.johjoh.nurcosmetics.sql.MySQLConnectionUtil;
import de.johjoh.nurcosmetics.util.item.ItemCreator;

public class ChestThread implements Runnable {
	
	private Thread thread;
	private int ticks;
	private ArrayList<Cosmetic> pool;
	private UUID player;
	private boolean running;
	private int count;
	private Cosmetic win;
	
	public ChestThread(int ticks, ArrayList<Cosmetic> pool, UUID player) {
		this.thread = new Thread(this);
		this.ticks = ticks;
		this.pool = pool;
		this.player = player;
	}
	
	public void start() {
		this.running = true;
		if(running)
			this.thread.start();
	}
	
	@SuppressWarnings("deprecation")
	public void stop() {
		this.running = false;
		CosmeticPlayer cp = CosmeticStorage.getCosmeticPlayer(player);
		cp.getPlayer().playSound(cp.getPlayer().getEyeLocation(), Sound.LEVEL_UP, 1.0F, 1.0F);
		Connection con = null;
		try {
			con = MySQLConnectionUtil.getNewConnection();
			cp.buyCosmetic(win, false, con);
		} catch(SQLException e) {
			e.printStackTrace();
		}
		MySQLConnectionUtil.closeConnection(con);
		
		cp.getInventorys().updateSubInventory(cp, win.getCategory());
		
		try {
			Thread.sleep(2000);
		} catch(InterruptedException e) {
			e.printStackTrace();
		}
		if(cp.getPlayer().getOpenInventory().getTopInventory() != null && cp.getPlayer().getOpenInventory().getTopInventory().getTitle() != null && cp.getPlayer().getOpenInventory().getTopInventory().getTitle().equalsIgnoreCase(ChatColor.GOLD + "Kiste \u00f6ffnen")) {
			cp.getPlayer().closeInventory();
		}
		cp.getInventorys().resetChestInventory();
		this.thread.stop();
	}
	
	public void run() {
		CosmeticPlayer cp = CosmeticStorage.getCosmeticPlayer(player);
		cp.getInventorys().getChestInventory().setItem(31, ItemCreator.namedSkull(" ", "MHF_ArrowUp"));
		cp.getInventorys().getChestInventory().setItem(13, ItemCreator.namedSkull(" ", "MHF_ArrowDown"));
		int color = 0;
		while(running) {
			if(ticks == 0) {
				stop();
				return;
			}
			
			color++;
			if(color == 16)
				color = 1;
			
			
			for(int i = 0; i < 9; i++) {
				cp.getInventorys().getChestInventory().setItem(0 + i, ItemCreator.namedItem(Material.STAINED_GLASS_PANE, " ", (short) (color+i > 15 ? color+i-15 : color+i)));
				if(i+9 != 13) cp.getInventorys().getChestInventory().setItem(9 + i, ItemCreator.namedItem(Material.STAINED_GLASS_PANE, " ", (short) (color+i > 15 ? color+i-15 : color+i)));
				cp.getInventorys().getChestInventory().setItem(18 + i, pool.get((count + i)).getItemStack(true, false, false, false));
				if(i+27 != 31) cp.getInventorys().getChestInventory().setItem(27 + i, ItemCreator.namedItem(Material.STAINED_GLASS_PANE, " ", (short) (color+i > 15 ? color+i-15 : color+i)));
				cp.getInventorys().getChestInventory().setItem(36 + i, ItemCreator.namedItem(Material.STAINED_GLASS_PANE, " ", (short) (color+i > 15 ? color+i-15 : color+i)));
			}
			cp.getPlayer().playSound(cp.getPlayer().getEyeLocation(), Sound.NOTE_PLING, 1.0F, 1.0F);
			
			win = pool.get((count + 4));
			
			
			int sleep = 2000/(ticks != 0 ? ticks : 1)+75;
			try {
				Thread.sleep(sleep);
			} catch(InterruptedException e) {
				e.printStackTrace();
			}
			
			count++;
			if(count >= pool.size()-9) {
				count = 0;
			}
			
			
			ticks--;
			
			
		}
	}

}
