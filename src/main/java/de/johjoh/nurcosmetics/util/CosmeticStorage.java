package de.johjoh.nurcosmetics.util;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.UUID;

public class CosmeticStorage {
	
	private static HashMap<UUID, CosmeticPlayer> cosmeticStorage = new HashMap<UUID, CosmeticPlayer>();
	
	public static CosmeticPlayer getCosmeticPlayer(UUID uniqueId) {
		if(!cosmeticStorage.containsKey(uniqueId)) return null;
		return cosmeticStorage.get(uniqueId);
	}
	
	public static HashMap<UUID, CosmeticPlayer> getCosmeticPlayers() {
		return cosmeticStorage;
	}
	
	public static void storeSummoner(CosmeticPlayer cp) {
		cosmeticStorage.put(cp.getUniqueId(), cp);
	}
	
	public static void removeSummoner(UUID uniqueId) {
		cosmeticStorage.remove(uniqueId);
	}
	
	public static boolean isRegistered(UUID uniqueId) {
		return cosmeticStorage.containsKey(uniqueId);
	}
	
	public static void loadPlayer(UUID uniqueId, Connection con) throws SQLException {
		CosmeticPlayer cp = new CosmeticPlayer(uniqueId, con);
		CosmeticStorage.storeSummoner(cp);
	}
	
	public static void r() {}

}
