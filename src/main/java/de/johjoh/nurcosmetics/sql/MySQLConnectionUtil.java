package de.johjoh.nurcosmetics.sql;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.bukkit.configuration.file.YamlConfiguration;

import de.johjoh.nurcosmetics.NurCosmetics;

public class MySQLConnectionUtil {

	private static String host = "localhost";
	private static int port = 3306;
	private static String username = "root";
	private static String password = "password";
	private static String database = "database";
	
	public static boolean loadVars() {
		File folder = new File("plugins/Database/");
		if(!folder.exists()) {
			return false;
		}
		File file = new File("plugins/Database/config.yml");
		if(!file.exists()) {
			return false;
		}
		YamlConfiguration fileConfiguration = YamlConfiguration.loadConfiguration(file);
		
		host = fileConfiguration.getString("mysql.hostname");
		port = fileConfiguration.getInt("mysql.port");
		username = fileConfiguration.getString("mysql.username");
		password = fileConfiguration.getString("mysql.password");
		database = NurCosmetics.TABLE_COSMETIC;
		return true;
	}

	public static Connection getNewConnection() throws SQLException {
		return DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + database, username, password);
	}

	public static Connection getNewConnectionOrNull() {
		try {
			return getNewConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String getHost() {
		return host;
	}

	public static boolean isConnectionValid(Connection con) throws SQLException {
		if (con != null) {
			if (!con.isClosed()) {
				if (con.isValid(1)) {
					return true;
				}
				return false;
			}
			return false;
		}
		return false;
	}

	public static boolean closeResources(ResultSet rs, PreparedStatement st) {
		try {
			if (rs != null) {
				rs.close();
			}
			if (st != null) {
				st.close();
			}
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public static boolean closeConnection(Connection con) {
		try {
			if (con != null) {
				con.close();
			}
			con = null;
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public static boolean closeConnection(ResultSet rs) {
		try {
			return closeConnection(rs.getStatement().getConnection());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
}
