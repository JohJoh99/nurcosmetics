package de.johjoh.nurcosmetics.sql;

import java.sql.Connection;
import java.sql.SQLException;

import de.johjoh.nurcosmetics.NurCosmetics;
import net.nurserver.core.database.Database;
import net.nurserver.core.database.mysql.Repository;

public class NurMySQL {
	
	private Repository repository;
	
	public NurMySQL() throws SQLException {
		this.repository = Database.getRepository(NurCosmetics.TABLE_COSMETIC, NurCosmetics.TABLE_COSMETIC);
//		CreateTables.createAllTables();
	}
	
	public Repository getRepository() {
		return this.repository;
	}
	
	public Connection getConnection() {
		return repository.getSql().getMySQL().getConnection();
	}

}
