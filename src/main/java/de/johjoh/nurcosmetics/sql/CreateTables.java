package de.johjoh.nurcosmetics.sql;

import static de.johjoh.nurcosmetics.NurCosmetics.COLUMN_CHESTS;
import static de.johjoh.nurcosmetics.NurCosmetics.COLUMN_COSMETICS;
import static de.johjoh.nurcosmetics.NurCosmetics.COLUMN_ID;
import static de.johjoh.nurcosmetics.NurCosmetics.COLUMN_PLAYER_UUID;
import static de.johjoh.nurcosmetics.NurCosmetics.TABLE_COSMETIC;

import java.sql.Connection;
import java.sql.SQLException;

public class CreateTables {
	
	public static void createAllTables(Connection con) throws SQLException {
		createCosmeticTable(con);
	}
	
	public static void createCosmeticTable(Connection con) throws SQLException {
		TableCreator tc = new TableCreator(TABLE_COSMETIC).ifNotExists().primary(COLUMN_ID).unique(COLUMN_PLAYER_UUID).engine("MyISAM").defaultCharset("utf8");
		tc.column(COLUMN_ID, "int(11)", true, "AUTO_INCREMENT");
		tc.column(COLUMN_PLAYER_UUID, "char(36)", true, null);
		tc.column(COLUMN_COSMETICS, "text", false, null);
		tc.column(COLUMN_CHESTS, "int(11)", true, "0");
		
		con.prepareStatement(tc.getStatement()).execute();
	}
}
