package de.johjoh.nurcosmetics.sql;

import java.util.ArrayList;
import java.util.Iterator;

public class TableCreator {
	
	private String tableName;
	private String ifNotExists = "";
	private ArrayList<String> cols = new ArrayList<String>();
	private String primary = "";
	private String unique = "";
	private String engine = "MyISAM";
	private String defaultCharset = "utf8";

	public TableCreator(String tableName) {
		this.tableName = tableName;
	}

	public TableCreator ifNotExists() {
		this.ifNotExists = " IF NOT EXISTS";
		return this;
	}

	public TableCreator column(String columnName, String type, boolean notNull, String defaultValue) {
		if(defaultValue != null) {
			defaultValue = defaultValue.trim();
		}
		String strNotNull = notNull ? "NOT NULL" : "NULL";
		String strDefVal = "";
		if (defaultValue != null) {
			if(defaultValue.equalsIgnoreCase("AUTO_INCREMENT")) {
				strDefVal = " " + defaultValue;
			} else if(defaultValue.equalsIgnoreCase("CURRENT_TIMESTAMP")) {
				strDefVal = " DEFAULT " + defaultValue;
			} else {
				strDefVal = " DEFAULT '" + defaultValue + "'";
			}
		}
		this.cols.add("`" + columnName + "` " + type + " " + strNotNull + strDefVal);
		return this;
	}

	public TableCreator unique(String unique) {
		this.unique = (", UNIQUE KEY `" + unique + "` (`" + unique + "`)");
		return this;
	}

	public TableCreator primary(String primary) {
		this.primary = (", PRIMARY KEY (`" + primary + "`)");
		return this;
	}

	public TableCreator engine(String engine) {
		this.engine = engine;
		return this;
	}

	public TableCreator defaultCharset(String defaultCharset) {
		this.defaultCharset = defaultCharset;
		return this;
	}

	public String getStatement() {
		String colStr = "";
		String col;
		for(@SuppressWarnings("rawtypes")
		Iterator localIterator = this.cols.iterator(); localIterator.hasNext(); colStr = colStr + col + ", ") {
			col = (String) localIterator.next();
		}
		if(colStr.length() > 0) {
			colStr = colStr.substring(0, colStr.length() - 2);
		}
		return "CREATE TABLE" + this.ifNotExists + " `" + this.tableName + "` (" + colStr + this.primary + this.unique + ") ENGINE=" + this.engine + " DEFAULT CHARSET=" + this.defaultCharset;
	}
	
}
